## Artifact Registry

Our dockerised services have to be accessible from our Google Cloud project. To do this we use Google's (Artifact Registry)[https://cloud.google.com/artifact-registry] Service. This allows us to create a repository where we can store our own repository Docker images.

#### Creating the Repository

We first need to create the repository:

`gcloud artifacts repositories create daily-repo --repository-format=docker --location=europe-west2 --description="Docker repository"`

#### Service Account Permission

We now need to create a service account with read-only access to the repository we made. This is so that our container instances and other resources can pull images from it.

    gcloud artifacts repositories add-iam-policy-binding daily-repo \
        --location=europe-west2
        --member=serviceAccount:438503002799-compute@developer.gserviceaccount.com
        --role="roles/artifactregistry.reader"

## Pushing Docker Images

Now that we have our own repository, we can push our custom images we defined in our (docker-compose.yml)[docker-compose.yml] file.

- The **image** tag contains the repository to push the image to, as well as the name and tag of the image.

First, we need to configure the Docker command-line tool to authenticate with the Artifact Registry:

` gcloud auth configure-docker europe-west2-docker.pkg.dev`

We can now push the images to our repository:

`docker compose push`

After the command has finished, you should see all our images on (Google Cloud)[https://console.cloud.google.com/artifacts/docker/daily-384822/europe-west2/daily-repo?project=daily-384822].

<br />

## Kubernetes Cluster

Kubernetes is an _amazing_ open-source container orchestration system for automating software deployment, scaling, and management. We use it to manage our services!#

First, we need to create a cluster:

`gcloud container clusters create-auto daily-cluster --region=europe-west2`

Verify the cluster is connected:

`gcloud container clusters get-credentials daily-cluster --region europe-west2`

The output should be: _kubeconfig entry generated for daily-cluster_. If so, you are now connected to the cluster!

<br />

**We can now move on to deploying our images!!! :D**

<br />

## Mongo Volumes

### Storage Class

Here we create a storage class that defines what properties the **default-mongo** class should have.

`kubectl apply -f gcloud/mongo-pv/mongo-storage-class.yaml`

You can check the current storage classes:

`kubectl get storageclasses`

### Creating Mongo Persistent Volumes

We need a storage system for the Mongo instances. For this, we use (Persistent Volumes)[https://kubernetes.io/docs/concepts/storage/persistent-volumes/]

- When we make a claim, Kubernetes automatically makes the Persistent Volume for us based on the Storage Class defined.

#### User Mongo Volume

`kubectl apply -f gcloud/user-mongo/user-mongo-pvc.yaml`

#### Friend Mongo Volume

`kubectl apply -f gcloud/friend-mongo/friend-mongo-pvc.yaml`

#### Feed Mongo Volume

`kubectl apply -f gcloud/feed-mongo/feed-mongo-pvc.yaml`

#### Comment Mongo Volume

`kubectl apply -f gcloud/comment-mongo/comment-mongo-pvc.yaml`

### Checking Result

You can check the current Persistent Volumes in our cluster:
`kubectl get pv`

As well as the current Persistent Volume Claims in our cluster:
`kubectl get pvc`

<br />

## Deploying Mongo Services

Now that we have the storage for our Mongo instances, we can deploy the services.

- The deployment defines that the instance should use our created volumes.

#### User Mongo Service

`kubectl apply -f gcloud/user-mongo/user-mongo-deployment.yaml`

#### Friend Mongo Service

`kubectl apply -f gcloud/friend-mongo/friend-mongo-deployment.yaml`

#### Feed Mongo Service

`kubectl apply -f gcloud/feed-mongo/feed-mongo-deployment.yaml`

#### Comment Mongo Service

`kubectl apply -f gcloud/comment-mongo/comment-mongo-deployment.yaml`

<br />

## Deploying Main Services

For our main services, we first define our Horizontal Autoscaler, then we deploy our service.

### Frontend

`kubectl apply -f gcloud/frontend-service/frontend-service-autoscaler.yaml`
`kubectl apply -f gcloud/frontend-service/frontend-service-deployment.yaml`

### User Backend

`kubectl apply -f gcloud/user-service/user-service-autoscaler.yaml`
`kubectl apply -f gcloud/user-service/user-service-deployment.yaml`

### Friend Backend

`kubectl apply -f gcloud/friend-service/friend-service-autoscaler.yaml`
`kubectl apply -f gcloud/friend-service/friend-service-deployment.yaml`

### Feed Backend

`kubectl apply -f gcloud/feed-service/feed-service-autoscaler.yaml`
`kubectl apply -f gcloud/feed-service/feed-service-deployment.yaml`

### Comment Backend

`kubectl apply -f gcloud/comment-service/comment-service-autoscaler.yaml`
`kubectl apply -f gcloud/comment-service/comment-service-deployment.yaml`

<br />

## NGINX Service

Thanks to our smort teammate Matt, we have a NGINX service. We use this service as a reverse proxy. What this means is that none of our services are exposed. Instead, we expose our NGINX service, and it manages our routes to services!

### ConfigMap

In Kubernetes, a ConfigMap is a key-value store that stores configuration data for your application. The ConfigMap can then be mounted as a volume inside a container, allowing the configuration data to be read by the application running inside the container.

So we define the config in the (nginx.conf)[gcloud\nginx-service\nginx.conf] file and deploy it:

`kubectl create configmap nginx-conf --from-file=gcloud/nginx-service/nginx.conf`

### Deploying NGINX Service

Before deploying the NGINX service, we define our Horizontal Autoscaler for it:
`kubectl apply -f gcloud/nginx-service/nginx-service-autoscaler.yaml`

We can now deploy NGINX as a service:

`kubectl apply -f gcloud/nginx-service/nginx-service-deployment.yaml`

What is different about this service, is that it is on a public endpoint!
