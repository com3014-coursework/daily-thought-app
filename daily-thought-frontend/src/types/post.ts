import { User } from "./user"

export type Post = {
  User: User;
  Content: string;
  Timestamp: string;
  Likes: User[];
}