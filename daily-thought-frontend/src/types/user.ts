export type User = {
  email: string,
  profile: string,
  username: string,
  id: string,
  firstName?: string,
  lastName?: string,
  admin: boolean,
}