export type StatusMessage = {
  message: string;
  error: boolean;
};
