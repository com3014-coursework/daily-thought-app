import { User } from "./user"

export type Comment = {
  Data: string,
  User: User,
  Likes: User[]
}