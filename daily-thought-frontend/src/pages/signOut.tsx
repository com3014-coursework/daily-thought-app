import Router from "next/router";
import { useEffect } from "react"

const SignOut = () => {

  useEffect(() => {
    if(sessionStorage.length > 0){
      sessionStorage.clear()
    }

    Router.push("/")
  })
  

  return (
    <>
      <title>Sign Out</title>
      <div className="w-screen h-screen flex justify-center items-center bg-c-pink">
        <p className="text-white text-xl">Bye!</p>
      </div>
    </>
  )
}

export default SignOut