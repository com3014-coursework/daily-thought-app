import Head from 'next/head';
import Image from 'next/image';
import { Inter } from 'next/font/google';
import NavBar from '@/components/navigation/NavBar';
import Hero from '@/components/hero/Hero';
import { useEffect } from 'react';
import Router from 'next/router';

const inter = Inter({ subsets: ['latin'] });

export default function Home() {
  useEffect(() => {
    if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('username') !== null) {
      Router.push('/feed');
    }
  });
  return (
    <>
      <Head>
        <title>Daily</title>
        <meta name="description" content="An application to share your thoughts" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className="min-h-screen bg-white">
        <NavBar user={undefined}>{<Hero />}</NavBar>
      </main>
    </>
  );
}
