import NavBar from '@/components/navigation/NavBar';
import { User } from '@/types/user';
import { PhotoIcon, UserCircleIcon } from '@heroicons/react/24/outline';
import Router from 'next/router';
import { FormEvent, useEffect, useState } from 'react';

const Profile = () => {
  const [user, setUser] = useState<undefined | User>(undefined);

  const fetchUser = async () => {
    const endpoint = `${process.env.NEXT_PUBLIC_USER_SERVICE_URL}api/user/${sessionStorage.getItem(
      'username'
    )}`;
    const response = await fetch(endpoint);
    return await response.json();
  };

  useEffect(() => {
    if (!user) {
      fetchUser().then((res) => {
        const { _id, username, email, profile, firstName, lastName, admin } = res;
        setUser({ id: _id, email, username, profile, firstName, lastName, admin });
      });
    }
  });

  const handleItemChange = (item: string, value: string) => {
    if (user) {
      const updated = { ...user, ...{ [item]: value } };
      setUser(updated);
    }
  };

  const handleSubmit = async (event: FormEvent) => {
    const JSONdata = JSON.stringify({
      username: user?.username,
      email: user?.email,
      profile: user?.profile,
      firstName: user?.firstName,
      lastName: user?.lastName
    });
    const endpoint = `${process.env.NEXT_PUBLIC_USER_SERVICE_URL}api/updateuser`;
    const options = {
      method: 'PUT',
      headers: {
        Authorization: `Bearer ${sessionStorage.getItem('token')}`,
        'Content-Type': 'application/json'
      },
      body: JSONdata
    };
    const response = await fetch(endpoint, options);
    const result = await response.json();
  };

  return (
    <>
      <title>Profile</title>
      <div className="w-full min-h-screen flex flex-col">
        <div className="w-full">
          <NavBar user={user} />
        </div>

        <form className="max-w-4xl mx-auto pt-10 p-3" onSubmit={(e) => handleSubmit(e)}>
          <div className="h-16"></div>
          <div className="space-y-12 w-full">
            <div className="border-b border-gray-900/10 pb-12">
              <h2 className="text-base font-semibold leading-7 text-gray-900">Profile</h2>
              <p className="mt-1 text-sm leading-6 text-gray-600">
                This information will be displayed publicly so be careful what you share.
              </p>

              <div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                <div className="sm:col-span-3">
                  <label
                    htmlFor="first-name"
                    className="block text-sm font-medium leading-6 text-gray-900"
                  >
                    First name
                  </label>
                  <div className="mt-2">
                    <input
                      type="text"
                      name="first-name"
                      id="first-name"
                      autoComplete="given-name"
                      className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                      value={user?.firstName}
                      onChange={(e) => handleItemChange('firstName', e.target.value)}
                    />
                  </div>
                </div>

                <div className="sm:col-span-3">
                  <label
                    htmlFor="last-name"
                    className="block text-sm font-medium leading-6 text-gray-900"
                  >
                    Last name
                  </label>
                  <div className="mt-2">
                    <input
                      type="text"
                      name="last-name"
                      id="last-name"
                      autoComplete="family-name"
                      className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                      value={user?.lastName}
                      onChange={(e) => handleItemChange('lastName', e.target.value)}
                    />
                  </div>
                </div>

                <div className="sm:col-span-4">
                  <label
                    htmlFor="username"
                    className="block text-sm font-medium leading-6 text-gray-900"
                  >
                    Username
                  </label>
                  <div className="mt-2">
                    <div className="flex bg-gray-100 rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                      <span className="flex select-none items-center pl-3 text-gray-500 sm:text-sm">
                        dialy.com/
                      </span>
                      <input
                        type="text"
                        name="username"
                        id="username"
                        autoComplete="username"
                        className="block flex-1 border-0 bg-transparent py-1.5 pl-0 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                        placeholder="janesmith"
                        value={user?.username}
                        onChange={(e) => handleItemChange('username', e.target.value)}
                        disabled
                      />
                    </div>
                  </div>
                </div>

                <div className="col-span-full">
                  <label
                    htmlFor="photo"
                    className="block text-sm font-medium leading-6 text-gray-900"
                  >
                    Photo
                  </label>
                  <div className="mt-2 flex items-center gap-x-3">
                    <UserCircleIcon className="h-12 w-12 text-gray-300" aria-hidden="true" />
                    <button
                      type="button"
                      className="rounded-md bg-white px-2.5 py-1.5 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50"
                    >
                      Change
                    </button>
                  </div>
                </div>
              </div>
            </div>

            <div className="border-b border-gray-900/10 pb-12">
              <h2 className="text-base font-semibold leading-7 text-gray-900">
                Personal Information
              </h2>
              <p className="mt-1 text-sm leading-6 text-gray-600">
                This won't be shared with anyone.
              </p>

              <div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                <div className="sm:col-span-4">
                  <label
                    htmlFor="email"
                    className="block text-sm font-medium leading-6 text-gray-900"
                  >
                    Email address
                  </label>
                  <div className="mt-2">
                    <input
                      id="email"
                      name="email"
                      type="email"
                      autoComplete="email"
                      className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                      value={user?.email}
                      onChange={(e) => handleItemChange('email', e.target.value)}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="mt-6 flex items-center justify-end gap-x-6">
            <button
              type="button"
              className="text-sm font-semibold leading-6 text-gray-900"
              onClick={() => {
                Router.push('/feed');
              }}
            >
              Cancel
            </button>
            <button
              type="submit"
              className="rounded-md bg-c-pink px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-c-green focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
            >
              Save
            </button>
          </div>
        </form>
      </div>
    </>
  );
};

export default Profile;
