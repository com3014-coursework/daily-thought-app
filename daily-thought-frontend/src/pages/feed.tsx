import NavBar from '@/components/navigation/NavBar';
import Post from '@/components/post/Post';
import { useUser } from '@/hooks/useUser';
import { useQuestion } from '@/hooks/useQuestion';
import QuestionSubmit from '@/components/form/QuestionSubmit';
import { useEffect, useState } from 'react';
import MenuTransition from '@/components/navigation/MenuTransition';
import DailyPostCompletion from '@/components/form/DailyCompletion';
import { FeedPost, useFeed } from '@/hooks/useFeed';
import QuestionCard from '@/components/user/QuestionCard';

const Feed = () => {
  const { user, setRehydrateUser } = useUser();
  const { question, rehydrateQuestion, setRehydrateQuestion } = useQuestion();
  const { feed, rehydrateFeed, feedUsers, setRehydrateFeed, setRehydrateFeedUsers } = useFeed({
    user,
    questionData: question
  });
  const [showQuestionForm, setShowQuestionForm] = useState<boolean>(false);
  const [postComplete, setPostComplete] = useState<boolean>(false);

  useEffect(() => {
    if (question === undefined && user === undefined) {
    } else {
      setRehydrateFeed(true);
    }
  }, [question, user]);

  useEffect(() => {
    if (feedUsers === undefined && feed !== undefined) {
      setRehydrateFeedUsers(true);
    }
  }, [feedUsers, feed]);

  const onQuestionSubmit = () => {
    setPostComplete(true);
    setRehydrateFeed(true);
  };

  return (
    <div className="w-full min-h-screen flex flex-col">
      <div className="">
        <NavBar user={user} rehydrateFeed={() => setRehydrateFeed(true)} />
      </div>
      <div className="flex w-full bg-c-pink flex-col items-center py-3 pt-20 justify-center items-center shadow-lg">
        <div className="px-4 flex-1 max-w-4xl flex items-center flex-col justify-center w-full ">
          {question !== undefined && user !== undefined && feed !== undefined && (
            <div className="max-w-4xl w-full justify-center flex">
              <QuestionCard
                questionData={question}
                user={user}
                posted={feed.userDaily !== null}
                action={() => setShowQuestionForm(true)}
              >
                {feedUsers !== undefined && (
                  <Post
                    loggedInUser={user}
                    post={feed.userDaily}
                    user={user}
                    lightText
                    question={question.question}
                    rehydrateFeed={() => setRehydrateFeed(true)}
                    userList={feedUsers}
                    openPostForm={() => setShowQuestionForm(true)}
                  />
                )}
              </QuestionCard>
            </div>
          )}
        </div>
      </div>

      {user && question && (
        <div>
          <QuestionSubmit
            questionData={question}
            user={user}
            onClose={() => setShowQuestionForm(false)}
            open={showQuestionForm}
            onSubmit={() => onQuestionSubmit()}
          />
          <DailyPostCompletion show={postComplete} />
        </div>
      )}

      <div className="flex-1 flex items-center flex-col justify-center mx-auto w-full h-full bg-gray-50">
        {(feed === undefined || feed?.feed.length === 0) && (
          <div className="bg-gray-50 flex flex-1 justify-center items-center w-full flex-col">
            <p className="text-sm text-gray-400">It's pretty quiet down here...</p>
            <a className="text-sm text-c-pink hover:text-c-green" href="/search">
              Find friends
            </a>
          </div>
        )}

        {feed !== undefined && feed?.feed.length > 0 && feedUsers !== undefined && (
          <div className="p-4 flex-1 max-w-2xl flex items-center flex-col w-full">
            <p className="text-sm text-c-pink">Your feed</p>
            <div className="max-w-4xl w-full justify-center flex">
              <div className="flex w-full justify-center flex-col items-center mx-auto p-3 sm:px-6 lg:px-8 max-w-4xl overflow-scroll">
                {feed.feed.map((feedPost: FeedPost) => {
                  const feedUser = feedUsers.get(feedPost.userId);

                  if (feedUser !== undefined && user !== undefined) {
                    return (
                      <Post
                        post={feedPost}
                        user={feedUser}
                        question={question.question}
                        rehydrateFeed={() => setRehydrateFeed(true)}
                        loggedInUser={user}
                        userList={feedUsers}
                        openPostForm={() => setShowQuestionForm(true)}
                      />
                    );
                  }
                })}
              </div>
            </div>
            <p className="text-sm text-gray-300">This is the end of your feed</p>
          </div>
        )}
      </div>
    </div>
  );
};

export default Feed;
