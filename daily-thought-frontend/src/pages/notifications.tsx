import FriendRequestMenu from "@/components/form/FriendRequests";
import NavBar from "@/components/navigation/NavBar";
import { useUser } from "@/hooks/useUser";

const Notifications = () => {
  const {user, setRehydrateUser} = useUser()
  return (
    <div>
      <NavBar user={user} /> 
        <div className="pt-20 p-3 max-w-4xl mx-auto">
          <FriendRequestMenu />
        </div>
    </div>
  )
}

export default Notifications;