import NavBar from '@/components/navigation/NavBar';
import { useQuestions } from '@/hooks/useQuestions';
import { useUser } from '@/hooks/useUser';
import { useState, useEffect } from 'react';
import Router from 'next/router';
import NewQuestionForm from '@/components/questions/NewQuestionForm';
import QuestionList from '@/components/questions/QuestionList';
import { StatusMessage } from '@/types/statusMessage';

const Questions = () => {
  const { user } = useUser();
  const { questions, setRehydrateQuestions } = useQuestions();
  const [newQuestion, setNewQuestion] = useState('');
  const [statusMessage, setStatusMessage] = useState<StatusMessage | undefined>(undefined);

  // Redirect user from page if not an admin
  useEffect(() => {
    if (user?.admin === false) {
      Router.push('/feed');
    }
  }, [user]);

  const insertNewQuestion = async (newQuestion: String): Promise<void> => {
    const JSONdata = JSON.stringify({ questionText: newQuestion });
    const endpoint = `${process.env.NEXT_PUBLIC_FEED_SERVICE_URL}insertQuestion`;
    const options = {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${sessionStorage.getItem('token')}`,
        'Content-Type': 'application/json'
      },
      body: JSONdata
    };

    var response = await fetch(endpoint, options);
    var message = await response.text();

    switch (response.status) {
      case 201:
        setStatusMessage({
          message: 'Question was successfully inserted!',
          error: false
        } as StatusMessage);
        break;
      case 400:
        setStatusMessage({ message: message, error: true } as StatusMessage);
        break;
      default:
        setStatusMessage({
          message: 'Unexpected error, please try again.',
          error: true
        } as StatusMessage);
        break;
    }
  };

  const deleteQuestion = async (questionId: String): Promise<void> => {
    const endpoint = `${process.env.NEXT_PUBLIC_FEED_SERVICE_URL}disableQuestion?questionId=${questionId}`;
    const options = {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${sessionStorage.getItem('token')}`,
        'Content-Type': 'application/json'
      }
    };

    await fetch(endpoint, options);
  };

  const onQuestionInsert = async (): Promise<void> => {
    await insertNewQuestion(newQuestion);
    setNewQuestion('');
    setRehydrateQuestions(true);
  };

  const onQuestionDelete = async (questionId: string): Promise<void> => {
    await deleteQuestion(questionId);
    setStatusMessage(undefined);
    setRehydrateQuestions(true);
  };

  return (
    <>
      <title>Questions</title>
      <div className="flex flex-col w-full items-center h-screen">
        {/* Navigation Bar */}
        <div className="w-full">
          <NavBar user={user} />
        </div>

        {/* Question Insert */}
        <NewQuestionForm
          newQuestion={newQuestion}
          setNewQuestion={setNewQuestion}
          onSubmit={onQuestionInsert}
          statusMessage={statusMessage}
        />

        {/* List of Questions */}
        <div className="flex flex-col items-center w-full shadow-lg h-full pb-4 overflow-auto">
          <h2 className="bg-gray-50 text-xl font-semibold leading-10 py-2 text-gray-900 w-full text-center">Questions Stored</h2>

          {questions === undefined ? (
            <p>Loading...</p>
          ) : (
            <QuestionList questions={questions} onDeleteClick={onQuestionDelete} />
          )}
        </div>
      </div>
    </>
  );
};

export default Questions;
