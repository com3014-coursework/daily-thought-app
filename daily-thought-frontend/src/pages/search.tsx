import UserSearch from "@/components/form/UserSearch";
import NavBar from "@/components/navigation/NavBar";
import { useFriends } from "@/hooks/useFriends";
import { useUser } from "@/hooks/useUser";

const Search = () => {
  const {user, setRehydrateUser} = useUser()

  return (
    <div>
      <title>Search</title>
      <NavBar user={user} /> 
        <div className="pt-20 p-3 max-w-4xl mx-auto">
          <UserSearch limit={100} />
        </div>
    </div>
  )
}

export default Search;