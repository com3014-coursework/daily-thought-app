import NavBar from '@/components/navigation/NavBar';
import Post from '@/components/post/Post';
import { useUser } from '@/hooks/useUser';
import { useQuestion } from '@/hooks/useQuestion';
import QuestionSubmit from '@/components/form/QuestionSubmit';
import { Fragment, useEffect, useState } from 'react';
import MenuTransition from '@/components/navigation/MenuTransition';
import DailyPostCompletion from '@/components/form/DailyCompletion';
import { FeedPost, useFeed } from '@/hooks/useFeed';
import QuestionCard from '@/components/user/QuestionCard';
import { useRouter } from 'next/router';
import { usePost } from '@/hooks/usePost';
import { Tab } from '@headlessui/react';
import UserList from '@/components/user/UserList';
import AvatarList from '@/components/user/AvatarList';
import Comment from '../../components/comments/Comment';
import { getComments } from '@/components/comments/GetComments';

const PostPage = () => {
  const router = useRouter();
  const { userId } = router.query;
  const { user, setRehydrateUser } = useUser();
  const { question, rehydrateQuestion, setRehydrateQuestion } = useQuestion();
  const { post, rehydratePost, postUsers, setRehydratePost, setRehydratePostUsers } = usePost({
    userId: userId as string
  });
  const [showQuestionForm, setShowQuestionForm] = useState<boolean>(false);
  const [postComplete, setPostComplete] = useState<boolean>(false);
  const [commentData, setCommentData] = useState<any[] | undefined>(undefined);

  useEffect(() => {
    if (commentData === undefined && post !== undefined) {
      getComments(post.id).then((res) => {
        setCommentData(res);
      });
    }
  }, [commentData, post, setCommentData]);

  const fetchCommentData = () => {
    if (post !== undefined) {
      getComments(post.id).then((res) => {
        setCommentData(res);
      });
    }
  };

  useEffect(() => {
    if (userId !== undefined && post === undefined) {
      setRehydratePost(true);
    }
  });

  useEffect(() => {
    if (post !== undefined) {
      setRehydratePost(true);
    }
  }, [question, user]);

  useEffect(() => {
    if (postUsers === undefined && post !== undefined) {
      setRehydratePostUsers(true);
    }
  }, [postUsers, post]);

  return (
    <>
      <title>{`${user?.username ?? ''}'s Daily`}</title>
      <div className="w-full h-screen flex flex-col bg-gray-50">
        <div className="">
          <NavBar user={user} />
        </div>
        <div className="flex w-full bg-c-pink flex-col items-center py-3 pt-20 justify-center items-center shadow-lg">
          <div className="px-4 flex-1 max-w-4xl flex items-center flex-col justify-center w-full ">
            {question !== undefined && user !== undefined && post !== undefined && (
              <div className="max-w-4xl w-full justify-center flex">
                <QuestionCard
                  questionData={question}
                  user={user}
                  posted={true}
                  action={() => null}
                />
              </div>
            )}
          </div>
          <div className="flex-1 px-4 max-w-xl flex items-center flex-col w-full mx-auto ">
            {post !== undefined && user && postUsers && (
              <Post
                loggedInUser={user}
                post={post}
                user={postUsers.get(userId as string) || user}
                lightText
                question={question.question}
                rehydrateFeed={() => setRehydratePost(true)}
                userList={postUsers}
                openPostForm={() => null}
                commentLimit={0}
                rehydrateComments={() => fetchCommentData()}
              />
            )}
          </div>
        </div>

        {post !== undefined && (
          <div className="w-full flex justify-center pt-4 bg-gray-50">
            <div className="flex flex-col items-center flex-shrink-1 w-full max-w-xl justify-center">
              <Tab.Group>
                <Tab.List className="inline-flex">
                  <div className="rounded-full shadow-lg bg-white">
                    <Tab as={Fragment}>
                      {({ selected }) => (
                        <button
                          className={`${
                            selected
                              ? 'bg-c-pink text-white border-c-pink'
                              : 'bg-gray-50 text-gray-400 hover:text-c-pink'
                          } w-[120px] rounded-full m-2 py-2 px-4 text-xs`}
                        >
                          {`Comments ${commentData ? commentData[0].length : 0}`}
                        </button>
                      )}
                    </Tab>
                    <Tab as={Fragment}>
                      {({ selected }) => (
                        <button
                          className={`${
                            selected
                              ? 'bg-c-pink text-white border-c-pink'
                              : 'bg-gray-50 text-gray-400 hover:text-c-pink'
                          } w-[120px] rounded-full m-2 py-2 px-4 text-xs`}
                        >
                          {`Likes ${post?.usersLiked.length}`}
                        </button>
                      )}
                    </Tab>
                  </div>
                </Tab.List>
                <Tab.Panels className="w-full">
                  <Tab.Panel>
                    <div className="w-full p-4">
                      {commentData !== undefined &&
                        commentData[0].map((commentItem: any) => {
                          const commentUser = commentData[1]?.get(commentItem.AuthorId);

                          if (commentUser && user) {
                            return (
                              <Comment
                                Comment={commentItem}
                                user={commentUser}
                                loggedInUser={user}
                                rehydrateComments={() => fetchCommentData()}
                              />
                            );
                          }
                        })}

                      {commentData !== undefined && commentData[0].length === 0 && (
                        <div className="text-center text-xs text-gray-400 py-2">
                          This post has 0 comments
                        </div>
                      )}
                    </div>
                  </Tab.Panel>
                  <Tab.Panel>
                    <div className="w-full p-4">
                      <AvatarList
                        Users={
                          postUsers !== undefined && post !== undefined
                            ? post?.usersLiked.map((id) => postUsers.get(id))
                            : []
                        }
                      ></AvatarList>
                    </div>
                  </Tab.Panel>
                </Tab.Panels>
              </Tab.Group>
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export default PostPage;
