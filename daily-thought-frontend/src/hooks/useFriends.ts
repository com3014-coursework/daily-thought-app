import { useState, useEffect } from "react"

export const useFriends = () => {
  const [rehydrateFriends, setRehydrateFriends] = useState(false);
  const [friends, setFriends] = useState<Map<string, any> | undefined>(undefined);

  useEffect(() => {
    if (friends !== undefined) return;
    setRehydrateFriends(true);
  }, [friends]);

  const fetchFriends = async () => {
    const endpoint = `${process.env.NEXT_PUBLIC_FRIEND_SERVICE_URL}friends`
    const headers =  {'Authorization': `Bearer ${sessionStorage.getItem("token")}`}
    const response = await fetch(endpoint, {headers})
    return await response.json()
  }

  const fetchUserList = async (userIdList: string[]) => {
    const endpoint = `${process.env.NEXT_PUBLIC_USER_SERVICE_URL}api/userlist`
    const JSONdata = JSON.stringify({userIdList})
    const options = {
      method: 'POST',
      headers: {'Content-Type': 'application/json',},
      body: JSONdata,
    }
    
    const response = await fetch(endpoint, options)
    return await response.json()
  }

  useEffect(() => {
    if (!rehydrateFriends) return;

    fetchFriends().then(res => {
      fetchUserList(res.result).then(userlistRes => {
        setFriends(new Map(userlistRes.userList))
        setRehydrateFriends(false)
      })
    })
  }, [rehydrateFriends])
  return {friends, rehydrateFriends, setRehydrateFriends} 
}