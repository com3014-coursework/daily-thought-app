import Router from 'next/router';
import { useState, useEffect } from 'react';

export const useQuestion = () => {
  const [rehydrateQuestion, setRehydrateQuestion] = useState(false);
  const [question, setQuestion] = useState<undefined | any>(undefined);

  useEffect(() => {
    if (question !== undefined) return;
    setRehydrateQuestion(true);
  }, [question]);

  const fetchQuestion = async () => {
    const endpoint = `${process.env.NEXT_PUBLIC_FEED_SERVICE_URL}question`;

    const response = await fetch(endpoint, { method: 'get' });
    if (response.ok) {
      const data = await response.json();
      return data;
    }
  };

  useEffect(() => {
    if (!rehydrateQuestion) return;

    fetchQuestion().then((res) => {
      setQuestion(res);
      setRehydrateQuestion(false);
    });
  }, [rehydrateQuestion]);
  return { question, rehydrateQuestion, setRehydrateQuestion };
};
