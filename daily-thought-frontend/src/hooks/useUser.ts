import { User } from '@/types/user';
import Router from 'next/router';
import { useState, useEffect } from 'react';

export const useUser = () => {
  const [rehydrateUser, setRehydrateUser] = useState(false);
  const [user, setUser] = useState<undefined | User>(undefined);

  useEffect(() => {
    if (user !== undefined) return;
    setRehydrateUser(true);
  }, [user]);

  const fetchUser = async () => {
    const endpoint = `${process.env.NEXT_PUBLIC_USER_SERVICE_URL}api/user/${sessionStorage.getItem(
      'username'
    )}`;
    const response = await fetch(endpoint);
    if (!response.ok) {
      sessionStorage.clear();
      Router.push('/');
    } else {
      const data = await response.json();
      return data;
    }
  };

  useEffect(() => {
    if (!rehydrateUser) return;

    fetchUser().then((res) => {
      try {
        const { _id, username, email, profile, firstName, lastName, admin } = res;
        setUser({ id: _id, email, username, profile, firstName, lastName, admin });
        setRehydrateUser(false);
      } catch (err){
        console.log(`Invalid session token: ${sessionStorage.getItem('username')}`)
        sessionStorage.clear();
      } 
    });
  }, [rehydrateUser]);
  return { user, setRehydrateUser };
};
