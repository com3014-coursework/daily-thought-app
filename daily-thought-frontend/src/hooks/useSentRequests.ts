import { useState, useEffect } from "react"

export const useSentRequests = () => {
  const [rehydrateSentRequests, setRehydrateSentRequests] = useState(false);
  const [sentRequests, setSetRequests] = useState<Map<string, any> | undefined>(undefined);

  useEffect(() => {
    if (sentRequests !== undefined) return;
    setRehydrateSentRequests(true);
  }, [sentRequests]);

  const fetchRequests = async () => {
    const endpoint = `${process.env.NEXT_PUBLIC_FRIEND_SERVICE_URL}friends/requests/sent`
    const headers =  {'Authorization': `Bearer ${sessionStorage.getItem("token")}`}
    const response = await fetch(endpoint, {headers})
    return await response.json()
  }


  useEffect(() => {
    if (!rehydrateSentRequests) return;

    fetchRequests().then(res => {
      const requestsHash = new Map<string, any>([]);
      res.requests.forEach((request: any) => {
        requestsHash.set(request.TargetUser, request)
      });
        setSetRequests(requestsHash)
        setRehydrateSentRequests(false);
      })
  }, [rehydrateSentRequests])
  return {sentRequests, rehydrateSentRequests, setRehydrateSentRequests}
}