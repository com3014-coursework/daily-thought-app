import { User } from '@/types/user';
import { useState, useEffect } from 'react';

type UseFeedProps = {
  user: User | undefined;
  questionData: any | undefined;
};

type FeedResponse = {
  userDaily: FeedPost;
  feed: [];
};

export type FeedPost = {
  id: string;
  userId: string;
  createdAt: string;
  content: string;
  questionId: string;
  usersLiked: string[];
};

export const useFeed = ({ user, questionData }: UseFeedProps) => {
  const [rehydrateFeed, setRehydrateFeed] = useState(false);
  const [feed, setFeed] = useState<FeedResponse | undefined>(undefined);
  const [feedUsers, setFeedUsers] = useState<Map<string, User> | undefined>(undefined);
  const [rehydrateFeedUsers, setRehydrateFeedUsers] = useState(false);

  useEffect(() => {
    if (feed !== undefined) return;
    setRehydrateFeed(true);
  }, [feed]);

  const fetchFeed = async () => {
    if (user !== undefined && questionData !== undefined) {
      const endpoint = `${process.env.NEXT_PUBLIC_FEED_SERVICE_URL}feed?userId=${user.id}&questionId=${questionData.id}`;
      const headers = { Authorization: `Bearer ${sessionStorage.getItem('token')}` };
      const response = await fetch(endpoint, { headers });

      if (!response.ok) {
      } else {
        return await response.json();
      }
    }
  };

  const fetchUserList = async (userIdList: string[]) => {
    const endpoint = `${process.env.NEXT_PUBLIC_USER_SERVICE_URL}api/userlist`;
    const JSONdata = JSON.stringify({ userIdList });
    const options = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSONdata
    };

    const response = await fetch(endpoint, options);
    return await response.json();
  };

  useEffect(() => {
    if (!rehydrateFeedUsers) return;

    const userSet = new Set<string>([]);
    if (user) {
      userSet.add(user.id);
    }

    feed?.feed.forEach((post: FeedPost) => {
      userSet.add(post.userId);
      post.usersLiked.forEach((userLikeId) => {
        userSet.add(userLikeId);
      });
    });

    if (userSet.size > 0) {
      fetchUserList(Array.from(userSet)).then((userListRes) => {
        const newFeedUsers = new Map<string, User>();
        userListRes.userList.forEach((item: any) => {
          newFeedUsers.set(item[1][0], { ...item[1][1], id: item[1][1]._id });
        });

        if (newFeedUsers.size === userSet.size) {
          setFeedUsers(newFeedUsers);
        }
      });
    }
    setRehydrateFeedUsers(false);
  });

  useEffect(() => {
    if (!rehydrateFeed) return;

    fetchFeed().then((res: FeedResponse) => {
      setFeed(res);
      setRehydrateFeed(false);
    });
  }, [rehydrateFeed]);

  return { feed, rehydrateFeed, feedUsers, setRehydrateFeed, setRehydrateFeedUsers };
};
