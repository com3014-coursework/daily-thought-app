import { useState, useEffect } from 'react';

export type QuestionRecord = {
  id: string;
  content: string;
  used: number;
  createdAt: string;
  updatedAt: string;
};

export const useQuestions = () => {
  const [rehydrateQuestions, setRehydrateQuestions] = useState(false);
  const [questions, setQuestions] = useState<undefined | QuestionRecord[]>(undefined);

  useEffect(() => {
    if (questions !== undefined) return;
    setRehydrateQuestions(true);
  }, [questions]);

  const fetchQuestions = async () => {
    const endpoint = `${process.env.NEXT_PUBLIC_FEED_SERVICE_URL}questions`;
    const headers = { Authorization: `Bearer ${sessionStorage.getItem('token')}` };

    const response = await fetch(endpoint, { headers });
    if (response.ok) {
      const data = await response.json();
      return data;
    }
  };

  useEffect(() => {
    if (!rehydrateQuestions) return;

    fetchQuestions().then((res: QuestionRecord[]) => {
      setQuestions(res);
      setRehydrateQuestions(false);
    });
  }, [rehydrateQuestions]);

  return { questions, setRehydrateQuestions };
};
