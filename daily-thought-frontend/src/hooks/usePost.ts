import { User } from '@/types/user';
import { useState, useEffect } from 'react';
import { FeedPost } from './useFeed';

type usePostProps = {
  userId: string;
};

export const usePost = ({ userId }: usePostProps) => {
  const [rehydratePost, setRehydratePost] = useState(false);
  const [post, setPost] = useState<FeedPost | undefined>(undefined);
  const [postUsers, setPostUsers] = useState<Map<string, User> | undefined>(undefined);
  const [rehydratePostUsers, setRehydratePostUsers] = useState(false);

  // useEffect(() => {
  //   if (post !== undefined) return;
  //   setRehydratePost(true);
  // }, [post]);

  const fetchPost = async () => {
    const endpoint = `${process.env.NEXT_PUBLIC_FEED_SERVICE_URL}user/currentDaily?userId=${userId}`;
    const headers = { Authorization: `Bearer ${sessionStorage.getItem('token')}` };
    const response = await fetch(endpoint, { headers });

    return await response.json();
  };

  const fetchUserList = async (userIdList: string[]) => {
    const endpoint = `${process.env.NEXT_PUBLIC_USER_SERVICE_URL}api/userlist`;
    const JSONdata = JSON.stringify({ userIdList });
    const options = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSONdata
    };

    const response = await fetch(endpoint, options);
    return await response.json();
  };

  useEffect(() => {
    if (!rehydratePostUsers) return;

    const userSet = new Set<string>([]);
    userSet.add(post?.userId as string);
    post?.usersLiked.forEach((userLikeId) => {
      userSet.add(userLikeId);
    });

    if (userSet.size > 0) {
      fetchUserList(Array.from(userSet)).then((userListRes) => {
        const newFeedUsers = new Map<string, any>();
        userListRes.userList.forEach((item: any) => {
          newFeedUsers.set(item[1][0], { ...item[1][1], id: item[1][1]._id });
        });

        if (newFeedUsers.size === userSet.size) {
          setPostUsers(newFeedUsers);
        }
      });
    }
    setRehydratePostUsers(false);
  });

  useEffect(() => {
    if (!rehydratePost) return;

    fetchPost().then((res) => {
      setPost(res);
      setRehydratePost(false);
    });
  }, [rehydratePost]);
  return {
    post,
    rehydratePost,
    postUsers,
    setRehydratePostUsers,
    setRehydratePost
  };
};
