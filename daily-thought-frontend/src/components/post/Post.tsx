import { FeedPost } from '@/hooks/useFeed';
import { User } from '@/types/user';

import { FC, PropsWithChildren } from 'react';
import UserAvatar from '../user/UserAvatar';
import AnswerCard from './AnswerCard';
import LikeStack from './LikeStack';
import { PostTimeHandler } from './utility';

type PostProps = {
  post: FeedPost;
  question: string;
  user: User;
  loggedInUser: User;
  lightText?: boolean;
  userList: Map<string, User>;
  rehydrateFeed: () => void;
  openPostForm: () => void;
  commentLimit?: number;
  rehydrateComments?: () => void;
};

const Post: FC<PropsWithChildren<PostProps>> = ({
  post,
  user,
  lightText = false,
  question,
  userList,
  loggedInUser,
  commentLimit = 1,
  rehydrateFeed,
  openPostForm,
  rehydrateComments
}) => {
  return (
    <>
      <div className="flex w-full flex-col py-4">
        <div className="flex items-end justify-between">
          <UserAvatar
            useLightText={lightText}
            username={user.username}
            firstName={user.firstName}
            lastName={user.lastName}
            id={user.id}
          />
          <div
            className={`flex items-center text-xs mr-4 mb-1 ${
              lightText ? 'text-gray-50' : 'text-gray-400'
            }`}
          >{`${PostTimeHandler(post.createdAt)}`}</div>
        </div>

        <div className="flex-1 rounded-3xl font-white md:ml-10 m-2">
          {post.content.length > 0 && (
            <AnswerCard
              user={user}
              loggedInUser={loggedInUser}
              post={post}
              comments={[]}
              question={question}
              rehydrateFeed={() => rehydrateFeed()}
              userList={userList}
              commentLimit={commentLimit}
              rehydrateComments={rehydrateComments ? () => rehydrateComments() : undefined}
            />
          )}
          {!post.content && (
            <div className="w-full shadow-md relative px-7 py-6 bg-white ring-1 ring-gray-900/5 rounded-xl leading-none flex items-top justify-start space-x-6">
              <div className="w-full space-y-2">
                <p className="text-slate-800">Content Hidden</p>
                <p className="text-gray-400 text-sm">
                  Post a daily to view what your friends are saying!
                </p>
                <div className="w-full">
                  <button
                    type="button"
                    className="mx-auto rounded-md bg-c-pink px-3 py-2 mt-2 text-sm font-semibold text-white shadow-sm hover:bg-c-green focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                    onClick={() => openPostForm()}
                  >
                    Post Daily
                  </button>
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    </>
  );
};

export default Post;
