import { FC, PropsWithChildren, useEffect, useState } from 'react';
import { HeartIcon, ChatBubbleLeftIcon } from '@heroicons/react/24/outline';
import { HeartIcon as Heart } from '@heroicons/react/24/solid';
import LikeStack from './LikeStack';
import Comment from '../comments/Comment';
import { User } from '@/types/user';
import NewComment from '../form/NewComment';
import { FeedPost } from '@/hooks/useFeed';
import DailyPostCompletion from '../form/DailyCompletion';
import { getComments } from '../comments/GetComments';

type AnswerCardProps = {
  post: FeedPost;
  question: string;
  user: User;
  loggedInUser: User;
  userList: Map<string, User>;
  comments: any[];
  commentLimit?: number;
  rehydrateFeed: () => void;
  rehydrateComments?: () => void;
};

const AnswerCard: FC<PropsWithChildren<AnswerCardProps>> = ({
  post,
  user,
  question,
  userList,
  loggedInUser,
  commentLimit = 1,
  rehydrateFeed,
  rehydrateComments
}) => {
  const name = user.firstName ? user.firstName : '@' + user.username;
  const [showCommentForm, setShowCommentForm] = useState<boolean>(false);
  const [showCommentCompletion, setShowCommentCompletion] = useState<boolean>(false);
  const isLiked = post.usersLiked.includes(loggedInUser.id);
  const [commentData, setCommentData] = useState<any[] | undefined>(undefined);

  useEffect(() => {
    if (commentData === undefined && post !== undefined) {
      getComments(post.id).then((res) => {
        setCommentData(res);
      });
    }
  }, [commentData, post, setCommentData]);

  const fetchCommentData = () => {
    if (post !== undefined) {
      getComments(post.id).then((res) => {
        setCommentData(res);
      });
    }
  };

  const handleCommentCompletion = () => {
    setShowCommentCompletion(true);
    if (rehydrateComments !== undefined) {
      rehydrateComments();
    } else {
      fetchCommentData();
    }
  };

  const likePost = async () => {
    const endpoint = `${process.env.NEXT_PUBLIC_FEED_SERVICE_URL}daily/${
      isLiked ? 'unlike' : 'like'
    }`;
    const JSONdata = JSON.stringify({ dailyId: post.id, likerId: loggedInUser.id });
    const headers = {
      Authorization: `Bearer ${sessionStorage.getItem('token')}`,
      'Content-Type': 'application/json'
    };
    const response = await fetch(endpoint, { method: 'PUT', headers, body: JSONdata });

    if (!response.ok) {
      const data = await response.json();
    } else {
      rehydrateFeed();
    }
  };

  return (
    <div className="flex-1 rounded-3xl bg-white text-lg shadow-lg overflow-hidden static">
      <div className="px-6 py-6 pb-3">
        <a
          href={`/post/${user.id}`}
          className="hover:text-c-pink text-sm text-gray-500 mb-1"
        >{`${name}'s answer`}</a>
        <div className="text-gray-900">{post.content}</div>
        <div className="flex ">
          <div className="flex-1 mt-2">
            <button
              type="button"
              className=" mr-4  p-2 rounded-full bg-white text-c-pink hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-white focus:ring-offset-2 "
              onClick={() => likePost()}
            >
              {isLiked ? (
                <Heart className="h-6 w-6 text-c-pink" aria-hidden="true" />
              ) : (
                <HeartIcon className="h-6 w-6" aria-hidden="true" />
              )}
            </button>
            <button
              type="button"
              className="  p-2 rounded-full bg-white text-c-pink hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-white focus:ring-offset-2"
              onClick={() => setShowCommentForm(true)}
            >
              <ChatBubbleLeftIcon className="h-6 w-6" aria-hidden="true" />
            </button>
          </div>
          <LikeStack userList={userList} postLikes={post.usersLiked} />
        </div>
      </div>

      {commentLimit > 0 && commentData !== undefined && (
        <div className="border-t bg-gray-50 p-2 px-4 hover:bg-gray-100">
          {commentData[0].length > 0 && (
            <div>
              <a
                href={`/post/${user.id}`}
                className="text-xs text-gray-500 hover:text-c-pink"
              >{`View all ${commentData[0].length} comments`}</a>
              {commentData[0].map((comment: any, index: number) => {
                const commentUser = commentData[1].get(comment.AuthorId);

                if (commentUser && index < commentLimit) {
                  return (
                    <Comment
                      key={index}
                      Comment={comment}
                      user={commentUser}
                      loggedInUser={loggedInUser}
                      rehydrateComments={() => fetchCommentData()}
                    />
                  );
                }
              })}
            </div>
          )}
          {commentData[0].length === 0 && (
            <div className="w-full flex justify-between">
              <p className="p-2 text-xs text-gray-500">No comments yet!</p>
            </div>
          )}
        </div>
      )}
      <NewComment
        name={name}
        postId={post.id}
        userId={loggedInUser.id}
        show={showCommentForm}
        onClose={() => setShowCommentForm(false)}
        onSubmit={() => handleCommentCompletion()}
        question={question}
        postContent={post.content}
      />
      <DailyPostCompletion show={showCommentCompletion} title="Comment Posted!" secondary="" />
    </div>
  );
};

export default AnswerCard;
