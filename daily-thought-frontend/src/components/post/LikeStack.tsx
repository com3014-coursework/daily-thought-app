import { User } from '@/types/user';
import { UserCircleIcon } from '@heroicons/react/24/solid';
import { FC, PropsWithChildren } from 'react';
import UserList from '../user/UserList';

type LikeStackProps = {
  userList: Map<string, User>;
  postLikes: string[];
};

const LikeStack: FC<PropsWithChildren<LikeStackProps>> = ({ userList, postLikes }) => {
  return (
    <div className="flex items-end bg-gray-50 rounded-full items-center px-2 flex-shrink-1">
      {postLikes.length > 0 && (
        <div className="flex -space-x-1 overflow-hidden items-center">
          <div className="mx-2 text-xs text-c-pink ">{`${postLikes.length} Like${
            postLikes.length === 1 ? '' : 's'
          }`}</div>
          <UserList Users={userList !== undefined ? postLikes.map((id) => userList.get(id)) : []}>
            {postLikes.map((user, index) => {
              if (index < 3) {
                return (
                  <div key={index}>
                    <div className="rounded-full bg-white text-sm focus:outline-none focus:ring-2 focus:ring-white focus:ring-offset-2 focus:ring-offset-c-pink">
                      <UserCircleIcon className="h-6 w-6 rounded-full text-gray-300 bg-white flex items-center justify-center" />
                    </div>
                  </div>
                );
              } else if (index === 3) {
                return (
                  <div
                    key={index}
                    className="inline-block h-6 w-6 rounded-full ring-2 ring-white bg-gray-100 flex items-center justify-center"
                  >
                    <p className="text-c-pink text-xs">{`+${userList.size - 4}`}</p>
                  </div>
                );
              } else {
                return null;
              }
            })}
          </UserList>
        </div>
      )}

      {postLikes.length === 0 && (
        <div className="text-xs text-gray-400 mx-2">Be the first to like!</div>
      )}
    </div>
  );
};

export default LikeStack;
