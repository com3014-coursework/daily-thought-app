export const PostTimeHandler = (createdAt: string): string => {
  const createdAtTime = new Date(createdAt);
  const currentTime = new Date();
  const timeInterval = (createdAtTime.getTime() - currentTime.getTime()) / (1000 * 60);

  if (Math.abs(timeInterval) > 1) {
    const postedAt = `${singleDigitHandler(createdAtTime.getHours())}:${singleDigitHandler(
      createdAtTime.getMinutes()
    )}`;
    return `${
      createdAtTime.getUTCDate() === currentTime.getUTCDate() ? 'Today' : 'Yesterday'
    } ${postedAt}`;
  } else {
    return 'Just now';
  }
};

const singleDigitHandler = (input: number): string => (input > 9 ? `${input}` : `0${input}`);
