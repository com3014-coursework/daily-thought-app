import { Dispatch, FC, FormEvent, SetStateAction } from 'react';
import { StatusMessage } from '@/types/statusMessage';

type NewQuestionFormProps = {
  newQuestion: string;
  setNewQuestion: Dispatch<SetStateAction<string>>;
  onSubmit: () => Promise<void>;
  statusMessage: StatusMessage | undefined;
};

const NewQuestionForm: FC<NewQuestionFormProps> = ({
  newQuestion,
  setNewQuestion,
  onSubmit,
  statusMessage
}) => {
  // Set the colour of the status message
  const statusTextColour = statusMessage
    ? statusMessage.error
      ? 'text-red-500'
      : 'text-green-500'
    : '';

  const onFormSubmit = (e: FormEvent<HTMLFormElement>): void => {
    e.preventDefault();
    onSubmit();
  };

  return (
    <div className="flex flex-col items-center align-center py-3 pt-20 shadow-md w-full shrink-0 grow-0">
      {/* Title */}
      <h2 className="text-xl font-semibold leading-7 text-gray-900">New Question</h2>

      {/* Description */}
      <p className="mt-1 text-sm leading-6 text-gray-600 text-center">
        As an admin, you can input your own question for the system to choose from.
      </p>

      {/* Question Form */}
      <form className="flex flex-col align-center items-center pt-6 w-4/5 md:3/5" onSubmit={onFormSubmit}>
        {/* Text Input */}
        <input
          type="text"
          name="new-question"
          id="new-question"
          placeholder="A new exciting question..."
          className="block rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 
                     ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset 
                     focus:ring-indigo-600 sm:text-sm sm:leading-6 w-80 md:w-3/5 lg:w-1/2"
          value={newQuestion}
          onChange={(e) => setNewQuestion(e.target.value)}
        />

        {/* Status Message */}
        {statusMessage && (
          <p className={'mt-1 text-sm leading-6 text-center ' + statusTextColour}>
            {statusMessage.message}
          </p>
        )}

        {/* Submit Button */}
        <button
          type="submit"
          className="rounded-md bg-c-pink px-3 py-2 mt-6 text-sm font-semibold text-white shadow-sm hover:bg-c-green focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
        >
          Insert
        </button>
      </form>
    </div>
  );
};

export default NewQuestionForm;
