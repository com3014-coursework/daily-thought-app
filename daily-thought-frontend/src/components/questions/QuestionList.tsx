import { FC } from 'react';
import { QuestionRecord } from '@/hooks/useQuestions';
import { TrashIcon } from '@heroicons/react/24/outline';
import styles from '../../styles/Scrollbox.module.css';

type QuestionListProps = {
  questions: QuestionRecord[];
  onDeleteClick: (questionId: string) => void;
};

const QuestionList: FC<QuestionListProps> = ({ questions, onDeleteClick }) => (
  <ul
    role="list"
    className={`flex flex-col min-h-min max-w-lg overflow-y-scroll divide-y divide-gray-100 mt-4 min-w-[16rem] sm:min-w-[24rem] ${styles['scrollbox-shadows']}`}
  >
    {questions.map((question: QuestionRecord) => (
      <li
        key={question.id}
        className="flex flex-row items-center justify-between gap-x-6 py-5 px-4"
      >
        <p className="text-base font leading-6 text-gray-900">{question.content}</p>
        <TrashIcon className="shrink-0 h-4 w-4 cursor-pointer" color='grey' type='button' onClick={() => onDeleteClick(question.id)} />
      </li>
    ))}
  </ul>
);

export default QuestionList;
