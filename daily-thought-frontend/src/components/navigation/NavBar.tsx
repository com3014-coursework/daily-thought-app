import { FC, PropsWithChildren } from 'react';
import { Disclosure, Menu } from '@headlessui/react';
import { Bars3Icon, BellIcon, XMarkIcon } from '@heroicons/react/24/outline';
import { UserCircleIcon, MagnifyingGlassIcon } from '@heroicons/react/24/solid';
import { User } from '@/types/user';
import NavMenu from './NavMenu';
import UserSearch from '../form/UserSearch';
import MenuTransition from './MenuTransition';
import FriendRequestMenu from '../form/FriendRequests';
import Router from 'next/router';

const navigation = [{ name: 'My Feed', href: '/feed', current: true }];

const userNavigation = (isAdmin: Boolean) => {
  const list = [
    { name: 'Your Profile', href: '/profile' },
    { name: 'Friends', href: '/search' }
  ];

  isAdmin && list.push({ name: 'Questions', href: '/questions' });
  list.push({ name: 'Sign out', href: '/signOut' });
  return list;
};

function classNames(...classes: any) {
  return classes.filter(Boolean).join(' ');
}

type NavBarProps = {
  user: User | undefined;
  rehydrateFeed?: () => void;
};

const NavBar: FC<PropsWithChildren<NavBarProps>> = ({ children, user, rehydrateFeed }) => {
  return (
    <>
      <div className="min-h-full min-w-full">
        <Disclosure as="nav" className="bg-c-pink fixed w-full z-10">
          {({ open }) => (
            <>
              <div className="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
                <div className={`flex h-16 items-center justify-between`}>
                  {user !== undefined && (
                    <div className="flex items-center">
                      <div className="hidden md:block">
                        <div className="flex items-baseline space-x-4">
                          {navigation.map((item) => (
                            <a
                              key={item.name}
                              href={item.href}
                              className={classNames(
                                item.current
                                  ? 'bg-c-pink text-white'
                                  : 'text-c-pink hover:bg-c-pink hover:text-white',
                                'rounded-md px-3 py-2 text-sm font-medium'
                              )}
                              aria-current={item.current ? 'page' : undefined}
                            >
                              {item.name}
                            </a>
                          ))}
                        </div>
                      </div>
                    </div>
                  )}
                  <div className="pl-5 cursor-pointer" onClick={() => Router.push('/')}>
                    <h1 className="ml-5 text-2xl font-bold tracking-tight text-c-green">Daily</h1>
                  </div>

                  {user !== undefined && (
                    <div className="hidden md:block flex justify-end">
                      <div className="flex items-center justify-end">
                        <NavMenu
                          icon={<MagnifyingGlassIcon className="h-6 w-6" aria-hidden="true" />}
                          width={'w-64'}
                        >
                          <UserSearch />
                        </NavMenu>

                        <NavMenu
                          icon={<BellIcon className="h-6 w-6" aria-hidden="true" />}
                          width={'w-64'}
                        >
                          <FriendRequestMenu
                            rehydrateFeed={rehydrateFeed ? () => rehydrateFeed() : undefined}
                          />
                        </NavMenu>

                        {/* Profile dropdown */}
                        <Menu as="div" className="relative ml-5">
                          <div className="ml-1">
                            <Menu.Button className="flex max-w-xs items-center rounded-full bg-white text-sm focus:outline-none focus:ring-2 focus:ring-white focus:ring-offset-2 focus:ring-offset-c-pink">
                              <span className="sr-only">Open user menu</span>
                              {user && user.profile.length > 0 && (
                                <img className="h-8 w-8 rounded-full" src={user.profile} alt="" />
                              )}

                              {user && user.profile.length === 0 && (
                                <UserCircleIcon className="h-8 w-8 rounded-full text-gray-300 flex items-center justify-center">
                                  {user.username[0]}
                                </UserCircleIcon>
                              )}
                            </Menu.Button>
                          </div>
                          <MenuTransition>
                            <Menu.Items className="absolute right-0 z-10 mt-2 w-48 origin-top-right rounded-md bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                              <div className="w-full border-b">
                                <p className="block px-4 py-2 text-sm text-gray-700 font-bold">{`Hi, ${
                                  user?.firstName || user?.username
                                }`}</p>
                              </div>
                              {userNavigation(user.admin).map((item) => (
                                <Menu.Item key={item.name}>
                                  {({ active }) => (
                                    <a
                                      href={item.href}
                                      className={classNames(
                                        active ? 'bg-gray-100' : '',
                                        'block px-4 py-2 text-sm text-gray-700'
                                      )}
                                    >
                                      {item.name}
                                    </a>
                                  )}
                                </Menu.Item>
                              ))}
                            </Menu.Items>
                          </MenuTransition>
                        </Menu>
                      </div>
                    </div>
                  )}

                  {user !== undefined && (
                    <div className="-mr-2 flex md:hidden">
                      {/* Mobile menu button */}
                      <Disclosure.Button className="inline-flex items-center justify-center rounded-md bg-c-pink p-2 text-white hover:bg-white hover:text-c-pink focus:outline-none focus:ring-2 focus:ring-white focus:ring-offset-2 focus:ring-offset-c-pink">
                        <span className="sr-only">Open main menu</span>
                        {open ? (
                          <XMarkIcon className="block h-6 w-6" aria-hidden="true" />
                        ) : (
                          <Bars3Icon className="block h-6 w-6" aria-hidden="true" />
                        )}
                      </Disclosure.Button>
                    </div>
                  )}
                </div>
              </div>

              <MenuTransition>
                <Disclosure.Panel className="md:hidden">
                  <div className="px-2 pt-2 sm:px-3 bg-c-pink">
                    {navigation.map((item) => (
                      <Disclosure.Button
                        key={item.name}
                        as="a"
                        href={item.href}
                        className={classNames(
                          item.current
                            ? 'bg-c-pink text-white'
                            : 'text-gray-300 hover:bg-gray-700 hover:text-white',
                          'block rounded-md px-3 py-2 text-base font-medium'
                        )}
                        aria-current={item.current ? 'page' : undefined}
                      >
                        {item.name}
                      </Disclosure.Button>
                    ))}
                  </div>
                  <div className=" pt-4 pb-3 bg-white-50 shadow-lg">
                    <div className="flex items-center px-2 bg-white mx-3 py-2 rounded-md shadow-sm">
                      <div className="flex-shrink-0">
                        {user && user.profile.length > 0 && (
                          <img className="h-12 w-12 rounded-full" src={user?.profile} alt="" />
                        )}

                        {user && user.profile.length === 0 && (
                          <UserCircleIcon className="h-12 w-12 rounded-full text-gray-300 flex items-center justify-center">
                            {user.username[0]}
                          </UserCircleIcon>
                        )}
                      </div>
                      <div className="ml-3 flex flex-col justify-between h-full">
                        {user?.firstName && (
                          <div className="text-base font-medium leading-none text-grey-600">{`${
                            user?.firstName || ''
                          } ${user?.lastName || ''}`}</div>
                        )}
                        <div className="text-sm font-medium leading-none text-gray-400">{`@${user?.username}`}</div>
                      </div>
                      <button
                        type="button"
                        className="ml-auto flex items-center flex-shrink-0 rounded-full text-c-pink p-1 hover:text-c-pink hover:bg-white focus:outline-none focus:ring-2 focus:ring-white focus:ring-offset-2 focus:ring-offset-c-pink"
                        onClick={() => Router.push('/notifications')}
                      >
                        <span className="text-xs text-gray-400 mr-3">View friend requests</span>
                        <BellIcon className="h-6 w-6" aria-hidden="true" />
                      </button>
                    </div>
                    <div className="mt-4 space-y-1 px-2">
                      {userNavigation(user?.admin ?? false).map((item) => (
                        <Disclosure.Button
                          key={item.name}
                          as="a"
                          href={item.href}
                          className="block bg-c-pink rounded-md px-3 py-2 text-base font-medium text-white hover:bg-white hover:text-c-pink"
                        >
                          {item.name}
                        </Disclosure.Button>
                      ))}
                    </div>
                  </div>
                </Disclosure.Panel>
              </MenuTransition>
            </>
          )}
        </Disclosure>
        <main>
          <div className="mx-auto">{children}</div>
        </main>
      </div>
    </>
  );
};

export default NavBar;
