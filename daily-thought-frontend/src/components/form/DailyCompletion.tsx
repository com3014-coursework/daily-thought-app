import { Dialog, Transition } from '@headlessui/react';
import { CheckCircleIcon } from '@heroicons/react/24/outline';
import { FC, Fragment, PropsWithChildren, useEffect, useState } from 'react';

type DailyCompletionProps = {
  show: boolean;
  title?: string;
  secondary?: string;
};

const DailyPostCompletion: FC<PropsWithChildren<DailyCompletionProps>> = ({
  show,
  title = 'Your Daily has been posted!',
  secondary = 'Now have a look at what your friends have said :)'
}) => {
  const [isOpen, setIsOpen] = useState(false);
  const [done, setDone] = useState(false);

  useEffect(() => {
    if (!isOpen && show && !done) {
      setIsOpen(true);
      setDone(true);

      setTimeout(() => {
        setIsOpen(false);
      }, 3000);
    }
  });

  return (
    <>
      <Transition appear show={isOpen} as={Fragment}>
        <Dialog as="div" className="relative z-10" onClose={() => setIsOpen(false)}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black bg-opacity-25" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-y-auto">
            <div className="flex min-h-full items-center justify-center p-4 text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all">
                  <Dialog.Title
                    as="h3"
                    className="text-lg font-medium leading-6 text-gray-900 text-center"
                  >
                    <div className="w-full flex justify-center mb-2">
                      <CheckCircleIcon className="h-12 w-12 rounded-full  text-c-pink flex items-center justify-center" />
                    </div>
                    {title}
                  </Dialog.Title>
                  <div className="mt-2">
                    <p className="text-sm text-gray-500 text-center">{secondary}</p>
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </>
  );
};

export default DailyPostCompletion;
