import { useRequests } from '@/hooks/useRequests';
import { CheckIcon, XCircleIcon } from '@heroicons/react/24/outline';
import Router from 'next/router';
import { FC, PropsWithChildren, useEffect, useState } from 'react';
import UserAvatar from '../user/UserAvatar';

type navMenuProps = {
  rehydrateFeed?: () => void;
};

const FriendRequestMenu: FC<PropsWithChildren<navMenuProps>> = ({ rehydrateFeed }) => {
  const [userRequestList, setUserRequestList] = useState<Map<string, any>>(new Map());
  const { requests, rehydrateRequests, setRehydrateRequests } = useRequests();

  const fetchUserDetails = async () => {
    const endpoint = `${process.env.NEXT_PUBLIC_USER_SERVICE_URL}api/userlist`;
    if (requests) {
      const JsonData = JSON.stringify({ userIdList: Array.from(requests?.keys()) });

      const options = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JsonData
      };

      const response = await fetch(endpoint, options);
      const data = await response.json();

      setUserRequestList(new Map(data.userList));
    }
  };

  const handleRequest = async (userId: string, accept: boolean) => {
    const endpoint = `${process.env.NEXT_PUBLIC_FRIEND_SERVICE_URL}friends/requests/${
      accept ? 'accept' : 'reject'
    }`;
    const JsonData = JSON.stringify({ request_id: requests?.get(userId)._id });
    const options = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${sessionStorage.getItem('token')}`
      },
      body: JsonData
    };

    const response = await fetch(endpoint, options);
    if (!response.ok) {
      const data = await response.json();
    } else {
      setRehydrateRequests(true);
    }

    if (accept && rehydrateFeed !== undefined) {
      rehydrateFeed();
      Router.reload();
    }
  };

  useEffect(() => {
    fetchUserDetails();
  }, [rehydrateRequests]);

  return (
    <div className="p-1">
      <div className="text-xs text-gray-400 border-b w-full pb-1">Friend requests</div>

      <div>
        {Array.from(userRequestList?.values()).map((req, index) => {
          const { profile, username, firstName, lastName, id } = req[1];
          return (
            <div key={req[0]}>
              <div className="flex justify-between items-center">
                <UserAvatar username={username} firstName={firstName} lastName={lastName} id={id} />
                <div className="flex">
                  <button
                    type="button"
                    className="ml-auto mr-1 flex-shrink-1 rounded-full text-c-pink p-1 hover:text-c-green shadow focus:outline-none "
                    onClick={() => handleRequest(req[0], true)}
                  >
                    <CheckIcon className="h-6 w-6" aria-hidden="true" />
                  </button>
                  <button
                    type="button"
                    className="ml-auto mr-1 flex-shrink-1 rounded-full text-c-pink p-1 hover:text-gray-200 shadow focus:outline-none "
                    onClick={() => handleRequest(req[0], false)}
                  >
                    <XCircleIcon className="h-6 w-6" aria-hidden="true" />
                  </button>
                </div>
              </div>
            </div>
          );
        })}
      </div>

      {userRequestList.size === 0 && (
        <div className="text-xs text-gray-400 flex justify-center mt-2">
          {' '}
          No pending friend requests
        </div>
      )}
    </div>
  );
};

export default FriendRequestMenu;
