import useDebounce from '@/hooks/useDebounce';
import { useFriends } from '@/hooks/useFriends';
import { useSentRequests } from '@/hooks/useSentRequests';
import { useUser } from '@/hooks/useUser';
import { UserPlusIcon } from '@heroicons/react/24/outline';
import { FC, PropsWithChildren, useEffect, useState } from 'react';
import UserAvatar from '../user/UserAvatar';
import BasicField from './BasicField';

type UserSearchProps = {
  limit?: number;
};

const UserSearch: FC<PropsWithChildren<UserSearchProps>> = ({ limit = 6 }) => {
  const [searchQuery, setSearchQuery] = useState<string>('');
  const debouncedSearchQuery = useDebounce<string>(searchQuery, 500);
  const [searchResults, setSearchResults] = useState<Map<string, any>>(new Map([]));
  const [loading, setLoading] = useState<boolean>(false);
  const { sentRequests, rehydrateSentRequests, setRehydrateSentRequests } = useSentRequests();
  const { friends, rehydrateFriends, setRehydrateFriends } = useFriends();
  const [sentRequestsIds, setSentRequestIds] = useState<Set<string>>(new Set([]));
  const { user, setRehydrateUser } = useUser();

  useEffect(() => {
    if (sentRequests !== undefined) {
      setSentRequestIds(new Set(Array.from(sentRequests?.keys())));
    }
  }, [rehydrateSentRequests]);

  useEffect(() => {
    setLoading(true);
    if (searchQuery.length === 0) {
      setSearchResults(new Map([]));
    }
  }, [searchQuery, setSearchResults]);

  useEffect(() => {
    if (debouncedSearchQuery.length > 0) {
      const endpoint = `${process.env.NEXT_PUBLIC_USER_SERVICE_URL}api/search?searchQuery=${debouncedSearchQuery}`;
      fetch(endpoint).then(async (result) => {
        const data = await result.json();
        setLoading(false);
        const newResults = new Map<string, any>(data.result);
        if (user) newResults.delete(user?.id);
        setSearchResults(newResults);
      });
    }
  }, [debouncedSearchQuery]);

  const sendRequest = async (receiverId: string) => {
    const endpoint = `${process.env.NEXT_PUBLIC_FRIEND_SERVICE_URL}friends/requests`;
    const JSONdata = JSON.stringify({ receiver_id: receiverId });
    const headers = {
      Authorization: `Bearer ${sessionStorage.getItem('token')}`,
      'Content-Type': 'application/json'
    };
    const response = await fetch(endpoint, { method: 'POST', headers, body: JSONdata });

    if (!response.ok) {
      const data = await response.json();
    } else {
      setRehydrateSentRequests(true);
    }
  };

  return (
    <div>
      <BasicField
        name="search"
        placeholder="Search users"
        onChange={setSearchQuery}
        autocomplete={false}
      />
      <div className="border-t pt-2 flex item-center flex-col">
        {!searchQuery && (
          <div className="flex items-center justify-center mb-2">
            <p className="text-xs text-gray-500">Enter a search query to find friends</p>
          </div>
        )}
        <div className="border-b">
          {searchQuery &&
            !loading &&
            Array.from(searchResults.values()).map((result, index) => {
              if (index < limit && !friends?.has(result._id) && result._id !== user?.id) {
                return (
                  <div
                    className="flex justify-between items-center hover:bg-gray-100 rounded-md mb-2"
                    key={index}
                  >
                    <UserAvatar
                      key={result._id}
                      username={result.username}
                      firstName={result.firstName}
                      lastName={result.lastName}
                      id={result.id}
                    />

                    {!sentRequestsIds.has(result._id) && (
                      <button
                        type="button"
                        className="ml-auto mr-1 flex-shrink-1 rounded-full text-c-pink p-1 hover:text-gray-200 focus:outline-none "
                        onClick={() => sendRequest(result._id)}
                      >
                        <UserPlusIcon className="h-6 w-6" aria-hidden="true" />
                      </button>
                    )}

                    {sentRequestsIds.has(result._id) && (
                      <p className="text-xs text-gray-400 mr-3">Pending</p>
                    )}
                  </div>
                );
              }
            })}

          {searchQuery && loading && (
            <div className="flex justify-center mb-2">
              <div
                className="h-6 w-6 animate-spin text-gray-300 rounded-full border-4 border-solid border-current border-r-transparent align-[-0.125em] motion-reduce:animate-[spin_1.5s_linear_infinite]"
                role="status"
              ></div>
            </div>
          )}

          {searchQuery && !loading && searchResults.size === 0 && (
            <div>
              <p className="text-xs text-gray-500 flex justify-center mb-2">No results found</p>
            </div>
          )}
        </div>

        <div className="w-full pt-2 text-xs mx-auto flex text-c-pink justify-center">
          {friends && friends.size > 0 ? 'Friends' : 'No friends added'}
        </div>
        {friends &&
          Array.from(friends.values()).map((result, index) => {
            return (
              <div className="flex justify-between hover:bg-gray-100 rounded-md" key={index}>
                <UserAvatar
                  key={index}
                  username={result[1].username}
                  firstName={result[1].firstName}
                  lastName={result[1].lastName}
                  id={result[1].id}
                />
              </div>
            );
          })}
      </div>
    </div>
  );
};

export default UserSearch;
