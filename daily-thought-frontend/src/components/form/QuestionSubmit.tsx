import { User } from '@/types/user';
import { Dialog, Menu, Transition } from '@headlessui/react';
import { XMarkIcon } from '@heroicons/react/24/outline';
import { FC, FormEvent, Fragment, PropsWithChildren, useState } from 'react';
import UserAvatar from '../user/UserAvatar';
import DailyPostCompletion from './DailyCompletion';

type QuestionSubmitProps = {
  questionData: { question: string; questionId: string; id: string };
  user: User;
  open: boolean;
  onClose: () => void;
  onSubmit: () => void;
};

const QuestionSubmit: FC<PropsWithChildren<QuestionSubmitProps>> = ({
  questionData,
  user,
  open,
  onClose,
  onSubmit
}) => {
  const [dailyResponse, setDailyResponse] = useState<string>('');
  const [error, setError] = useState<string>('');

  const handleChange = (value: string) => {
    setDailyResponse(value);
  };

  const handleSubmit = async (e: FormEvent) => {
    e.preventDefault();
    if (!dailyResponse) {
      setError("Can't post an empty Daily!");
    } else {
      const endpoint = `${process.env.NEXT_PUBLIC_FEED_SERVICE_URL}daily/create`;
      const JsonData = JSON.stringify({
        content: dailyResponse,
        questionId: questionData.id,
        userId: user.id
      });
      const options = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${sessionStorage.getItem('token')}`
        },
        body: JsonData
      };
      const response = await fetch(endpoint, options);
      if (!response.ok) {
        const data = await response.statusText;
      } else {
        onSubmit();
        onClose();
      }
    }
  };

  return (
    <>
      <Transition appear show={open} as={Fragment}>
        <Dialog as="div" className="relative z-10" onClose={onClose}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black bg-opacity-25" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-y-auto">
            <div className="flex min-h-full items-center justify-center p-4 text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all">
                  <Dialog.Title as="h3" className="text-lg font-medium leading-6 text-gray-900">
                    <div className="flex w-full justify-between">
                      <div className="mb-3 flex flex-col justify-center">
                        <h1 className="text-2xl font-bold tracking-tight text-gray-900">Daily</h1>
                        <p className="text-sm font-normal text-gray-400">Today's question</p>
                      </div>

                      <button
                        type="button"
                        className="ml-auto  flex items-start flex-shrink-1 rounded-full text-gray-400 p-1 hover:text-c-pink hover:bg-white focus:outline-none"
                        onClick={() => onClose()}
                      >
                        <XMarkIcon className="h-6 w-6" aria-hidden="true" />
                      </button>
                    </div>

                    <div className="text-2xl font-bold tracking-tight text-center p-4 text-c-pink">
                      {questionData.question}
                    </div>
                  </Dialog.Title>

                  <div className="mt-2">
                    <form
                      className="px-2"
                      onSubmit={(e: FormEvent<HTMLFormElement>) => handleSubmit(e)}
                    >
                      <div className="mb-4">
                        <label
                          htmlFor="about"
                          className="block text-sm font-medium leading-6 text-sm text-gray-900"
                        >
                          Your response
                        </label>
                        <div className="mt-2">
                          <textarea
                            id="about"
                            name="about"
                            rows={2}
                            className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                            value={dailyResponse}
                            onChange={(e) => handleChange(e.target.value)}
                          />
                        </div>
                        <p className="text-xs text-c-pink m-1">{error}</p>
                      </div>

                      <button
                        type="submit"
                        className="rounded-md bg-c-pink px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-c-green focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                      >
                        Submit
                      </button>
                    </form>
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </>
  );
};

export default QuestionSubmit;
