import { FC } from "react"

type BasicFieldProps = {
  name: string,
  placeholder: string,
  error?: string | null
  autocomplete?: boolean
  onChange: (value: string) => void;
}

const BasicField:FC<BasicFieldProps> = ({
  name,
  placeholder,
  error,
  autocomplete = true,
  onChange
}) => {

  const handleChange = (value: string) => {
    onChange(value)
  }

  return (
    <div className="mb-3">
      <label htmlFor={name} className="sr-only">
        {placeholder}
      </label>
      <input
        id={name}
        name={name}
        type={name}
        autoComplete={autocomplete ? name : 'off'}
        required
        className={`p-3 relative block w-full rounded-md border-0 py-1.5 text-gray-900 ring-1 ring-inset placeholder:text-gray-400 focus:z-10  sm:text-sm sm:leading-6 ${error ? "ring-c-pink" : "ring-gray-300"}`}
        placeholder={placeholder}
        onChange={e => handleChange(e.target.value)}
      />
      <p className="text-xs text-c-pink m-1">
        {error}
      </p>
    </div>
  )
}

export default BasicField