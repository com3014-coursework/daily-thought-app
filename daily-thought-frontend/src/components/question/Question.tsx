const Question = () => {

  return (
      <div className="max-w-2xl lg:max-w-4xl border border-pink-400 border-2 p-5 rounded-full m-3">
          <blockquote className="text-center text-xl font-semibold leading-8 text-pink-400 sm:text-2xl sm:leading-9">
            <p>
              <span className="text-green-400 p-2 text-5xl">"</span>Lorem ipsum dolor sit amet consectetur adipisicing elit.<span className="text-green-400 p-2 text-5xl">"</span>
            </p>
          </blockquote>
      </div>
  )
}

export default Question;