import { User } from '@/types/user';

export type UserComment = {
  PostId: string;
  AuthorId: string;
  Content: string;
  LikerIds: string[];
  _id: string;
};

export const getComments = async (postId: string): Promise<[UserComment[], Map<string, User>]> => {
  let comments: UserComment[] = [];
  let commentUsers: Map<string, User> = new Map();
  const userSet = new Set<string>([]);

  return fetchComments(postId).then((commentsRes: { result: UserComment[] }) => {
    comments = commentsRes.result;

    comments.forEach((comment) => {
      userSet.add(comment.AuthorId);
      comment.LikerIds.forEach((liker) => {
        userSet.add(liker);
      });
    });

    return fetchUserList(Array.from(userSet)).then((userListRes) => {
      const newCommentUsers = new Map<string, any>();
      userListRes.userList.forEach((item: any) => {
        newCommentUsers.set(item[1][0], { ...item[1][1], id: item[1][1]._id });
      });

      if (newCommentUsers.size === userSet.size) {
        commentUsers = newCommentUsers;
      }

      return [comments, commentUsers];
    });
  });
};

export const fetchComments = async (postId: string) => {
  const endpoint = `${process.env.NEXT_PUBLIC_COMMENT_SERVICE_URL}comment/${postId}`;
  const headers = { Authorization: `Bearer ${sessionStorage.getItem('token')}` };
  const response = await fetch(endpoint, { headers });
  return await response.json();
};

export const fetchUserList = async (userIdList: string[]) => {
  const endpoint = `${process.env.NEXT_PUBLIC_USER_SERVICE_URL}api/userlist`;
  const JSONdata = JSON.stringify({ userIdList });
  const options = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSONdata
  };

  const response = await fetch(endpoint, options);
  return await response.json();
};
