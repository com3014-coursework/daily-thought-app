import { User } from '@/types/user';
import { HeartIcon } from '@heroicons/react/24/outline';
import { HeartIcon as Heart, UserCircleIcon } from '@heroicons/react/24/solid';
import { FC, PropsWithChildren, useState } from 'react';
import { UserComment } from './GetComments';

type CommentProps = {
  Comment: UserComment;
  user: User;
  loggedInUser: User;
  rehydrateComments: () => void;
};

const Comment: FC<PropsWithChildren<CommentProps>> = ({
  Comment,
  user,
  loggedInUser,
  rehydrateComments
}) => {
  const isLiked = Comment.LikerIds.includes(loggedInUser.id);

  const likeComment = async () => {
    const endpoint = `${process.env.NEXT_PUBLIC_COMMENT_SERVICE_URL}comment/${
      isLiked ? 'unlike' : 'like'
    }`;
    const JSONdata = JSON.stringify({ commentId: Comment._id, userId: loggedInUser.id });
    const headers = {
      Authorization: `Bearer ${sessionStorage.getItem('token')}`,
      'Content-Type': 'application/json'
    };
    const response = await fetch(endpoint, { method: 'POST', headers, body: JSONdata });

    if (!response.ok) {
      const data = await response.json();
    } else {
      rehydrateComments();
    }
  };

  return (
    <div className="w-full flex items-center justify-between p-2">
      <div className="flex flex-1">
        <div className="flex h-8 w-8 max-w-xs items-center justify-center rounded-full text-sm focus:outline-none focus:ring-2 focus:ring-white focus:ring-offset-2 focus:ring-offset-pink-400 mr-2">
          <UserCircleIcon className="h-8 w-8 rounded-full text-gray-300 bg-white flex items-center justify-center" />
        </div>
        <div>
          <p className="text-xs font-bold">{user.username}</p>
          <p className="text-sm">{Comment.Content}</p>
          <p className="text-xs text-gray-500">{`${Comment.LikerIds.length} Like${
            Comment.LikerIds.length === 1 ? '' : 's'
          }`}</p>
        </div>
      </div>

      {!isLiked ? (
        <HeartIcon
          className="block h-5 w-5 hover:text-c-pink text-gray-400"
          aria-hidden="true"
          onClick={() => likeComment()}
        />
      ) : (
        <Heart
          className="block h-5 w-5 text-c-pink"
          aria-hidden="true"
          onClick={() => likeComment()}
        />
      )}
    </div>
  );
};

export default Comment;
