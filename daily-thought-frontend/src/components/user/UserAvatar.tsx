import { UserCircleIcon, UserPlusIcon } from '@heroicons/react/24/solid';
import Router from 'next/router';
import { FC, PropsWithChildren } from 'react';

type UserAvatarProps = {
  useLightText?: boolean;
  firstName?: string;
  lastName?: string;
  profile?: string;
  username: string;
  id: string;
  bg?: string;
};

const UserAvatar: FC<PropsWithChildren<UserAvatarProps>> = ({
  useLightText = false,
  username,
  firstName,
  lastName,
  id,
  bg = 'bg-transparent'
}) => {
  const userNameClasses = firstName ? 'text-sm' : 'ml-1 text-md';

  return (
    <div className={`flex items-center p-1 ${bg}`}>
      <div className="rounded-full bg-white text-sm focus:outline-none focus:ring-2 focus:ring-white focus:ring-offset-2 focus:ring-offset-c-pink">
        <UserCircleIcon className="h-12 w-12 rounded-full text-gray-300 bg-white flex items-center justify-center" />
      </div>
      <div
        className={`text-sm ml-1 ${
          useLightText ? 'text-white' : 'text-gray-900'
        } whitespace-nowrap`}
      >
        {firstName && <div>{`${firstName} ${lastName || null}`}</div>}
        <p className={`${userNameClasses} ${useLightText ? 'text-gray-100' : 'text-gray-500'}`}>
          @{username}
        </p>
      </div>
    </div>
  );
};

export default UserAvatar;
