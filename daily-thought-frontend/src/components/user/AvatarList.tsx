import { User } from '@/types/user';
import { FC } from 'react';
import UserAvatar from './UserAvatar';

type AvatarListProps = {
  Users: (User | undefined)[];
};

const AvatarList: FC<AvatarListProps> = ({ Users }) => {
  return (
    <div>
      {Users && (
        <div className="">
          {Users.map((user) => {
            if (user !== undefined) {
              return (
                <UserAvatar
                  username={user.username}
                  firstName={user.firstName}
                  lastName={user.lastName}
                  id={user.id}
                  bg="bg-white rounded-full"
                />
              );
            }
          })}

          {Users.length === 0 && (
            <div className="w-full flex justify-center">
              <div className=" text-xs text-gray-400">This post has 0 likes</div>
            </div>
          )}
        </div>
      )}
    </div>
  );
};

export default AvatarList;
