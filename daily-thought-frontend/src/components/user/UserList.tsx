import { User } from '@/types/user';
import { Popover, Transition } from '@headlessui/react';
import { ChevronDownIcon } from '@heroicons/react/24/outline';
import { FC, Fragment, PropsWithChildren } from 'react';
import UserSearch from '../form/UserSearch';
import AvatarList from './AvatarList';
import UserAvatar from './UserAvatar';

type UserListProps = {
  Users: (User | undefined)[];
};

const UserList: FC<PropsWithChildren<UserListProps>> = ({ Users, children }) => {
  return (
    <div className="m-3">
      <Popover className="flex">
        <Popover.Button>
          <div className="flex -space-x-1 overflow-hidden relative items-center">{children}</div>
        </Popover.Button>

        <Transition
          as={Fragment}
          enter="transition ease-out duration-100"
          enterFrom="opacity-0 translate-y-1"
          enterTo="opacity-100 translate-y-0"
          leave="transition ease-in duration-150"
          leaveFrom="opacity-100 translate-y-0"
          leaveTo="opacity-0 translate-y-1"
        >
          <Popover.Panel className="max-w-[14rem] absolute z-50 left-2/3 mt-8 w-screen w-60 -translate-x-1/2 transform px-4 sm:px-0">
            <div className=" relative overflow-hidden rounded-lg shadow-lg ring-1 ring-black ring-opacity-5">
              <div className="bg-white p-3">
                <span className="flex items-center border-b">
                  <span className="text-xs font-medium text-gray-400 mb-1">{`${Users.length} Like${
                    Users.length === 1 ? '' : 's'
                  }`}</span>
                </span>
                <AvatarList Users={Users} />
              </div>
            </div>
          </Popover.Panel>
        </Transition>
      </Popover>
    </div>
  );
};

export default UserList;
