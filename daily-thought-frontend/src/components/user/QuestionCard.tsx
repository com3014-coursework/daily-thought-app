import { User } from '@/types/user';
import { FC, PropsWithChildren } from 'react';

type QuestionCardProps = {
  questionData: any;
  posted: boolean;
  user: User;
  action: () => void;
};

const QuestionCard: FC<PropsWithChildren<QuestionCardProps>> = ({
  questionData,
  posted,
  user,
  children,
  action
}) => {
  return (
    <div className="flex-1 max-w-xl flex flex-col justify-center">
      <div className="flex rounded-full my-4 m-2">
        <div className="flex w-full flex-col">
          <div className="flex w-full justify-between">
            <div className="mb-3 flex flex-col justify-center">
              <h1 className="text-2xl font-bold tracking-tight text-gray-900 text-white">Daily</h1>
              <p className="text-sm font-normal text-white">Today's question</p>
            </div>
          </div>

          <div className="sm:ml-6 flex-1 rounded-3xl font-white ">
            <div className=" transform rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all m-2">
              <div className="text-lg font-medium leading-6 text-gray-900">
                <div className="flex justify-center items-center flex-col">
                  <div className="text-2xl font-bold tracking-tight text-center text-c-pink">
                    {questionData.question}
                  </div>
                </div>
              </div>
            </div>
          </div>
          {!posted && (
            <div className="flex justify-center w-full pt-4">
              <button
                type="button"
                className="rounded-md bg-white px-3 py-2 text-sm font-semibold text-c-pink shadow-sm hover:bg-c-green hover:text-white focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                onClick={() => action()}
              >
                Answer
              </button>
            </div>
          )}
        </div>

        <div></div>
      </div>
      {posted && <div className="p-2 pt-0">{children}</div>}
    </div>
  );
};

export default QuestionCard;
