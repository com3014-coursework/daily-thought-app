# How To: Contribute to this Repo
This documentation article provides a simple explaination about how to contribute to this repo covering all the GitLab and VSCode ends of things.

## Step 1: Create an Issue
What are you trying to do? 
Create an issue
<img src="Images/Screenshot 2023-03-10 at 12.14.58.png"/>

Fill out the details
<img src="Images/Screenshot 2023-03-10 at 12.30.33.png" />

## Step 2: Create a merge request (MR)
This will create the bracnh that you'll use to develop on.

Click this:
<img  src="Images/Screenshot 2023-03-10 at 12.32.53.png"/>

Fill out the details:
<img src="Images/Screenshot 2023-03-10 at 12.36.35.png" />

<img src="Images/Screenshot 2023-03-17 at 19.24.06.png" />

## Step 3: Checkout the branch & Make the change
Click the button to copy the branch name
<img src="Images/Screenshot 2023-03-17 at 17.11.44.png">

- Open Your code editior, in the case Visual Studio Code (VSCode) and use git to checkout that branch:

- In VSCode hit CRTL (Or CMD)  + Shift + P to open the Command Palette. 

- Type "Git: Checkout" and select "Git: Checkout to..." 

<img src="Images/Screenshot 2023-03-17 at 17.18.33.png"/>

Next, enter origin/yourbranchnamehere and hit enter to checkout your branch. With your branch checked out, you can now make changes to your local copy of the branch. 

<img src="Images/Screenshot 2023-03-17 at 17.20.13.png"/>

Commit the changes, and push the changes to sync your local branch with the Gitlab copy (the origin).

## Step 4: Request a merge review

Finilay, Once you are happy with your changes, complete your MR and assign it as "ready" for reviewing. 

<img src="Images/Screenshot 2023-03-17 at 17.25.44.png"/>

## Step 5: Done! Send a message on the chat asing for a review