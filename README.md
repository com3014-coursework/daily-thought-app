# Daily - A Web Application

Application URL: http://35.230.140.48/.

Everyday at a random time between the morning and the afternoon, a new question is released - this is called the Daily Question. The mind-bending, head-scratching question is provided to all the users so they can answer it. A user's answer is called a **Daily**, and they can only post one Daily per question. Using the feed, a user can see all of their friend's intriguing, fresh, and controversial Dailies.They can read, comment, and like their friends' Dailies as they please.

## Members
**All members of Group 6:**
- Shaikh Rezwan Rafid Ahmad 6732715
- Felipe D'Abrantes 6569390
- Matt Kirby 6572050
- Venkatesh Nagasubramanian 6721597
- Patrick Talty-Kerr 6583333

## Services

| Service  | Description                       | Technology           |
|----------|-----------------------------------|----------------------|
| User     | Manages users.                    | Node.js              |
| Friends  | Manages friendships and requests. | Node.js              |
| Question | Manages the daily questions.      | Node.js              |
| Feed     | Manages the Dailies.              | Scala Play Framework |
| Comment  | Manages the comments of Dailies.  | Node.js              |

## Other Technologies Used

- MongoDB: Database system.
- Docker: Containerization tool for containerising services.
- Kubernetes: Container orchestration tool for scaling and managing the containers.
- NGINX: Reverse proxy for handling incoming requests.
- Google Cloud Platform: Cloud infrastructure for deploying the application.

## Running Locally

Navigate to root folder and run:

`docker compose up --build`

Application is exposed on http://localhost:8080/.

## Deployment

Instructions for deployment on Google Cloud can be found on [Deployment Steps](./gcloud/Deployment%20Steps.md).
