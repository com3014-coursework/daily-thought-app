import express, { Application } from "express";
import { initializeRoutes } from "./src/Startup/routes";
import { MongoConnectionProvider } from "./src/Database/MongoConnectionProvider";
import cors from "cors";
import jwt from "jsonwebtoken";

// Server configuration
const server: Application = express();
server.use(cors());

// InitaliseRoutes
initializeRoutes(server);

// Database configuration
export const MongoClient = new MongoConnectionProvider(
  process.env.MONGO_DBNAME as string,
  process.env.MONGO_HOST as string,
  Number(process.env.MONGO_PORT as string)
);
MongoClient.Connect();

// MongoClient.Connection.dropDatabase()
console.log(jwt.sign("a", "abcdefg12345"));
console.log(jwt.sign("b", "abcdefg12345"));
console.log(jwt.sign("c", "abcdefg12345"));

server.listen(9000, (): void => {
  console.log("Service: Running here 👉 https://localhost:9000");
});
