import express, { Application } from "express";
import { authorize } from "../Middleware/Auth";
import { IndexRouter } from "../Routes";
import { CommentsRouter } from "../Routes/CommentsRouter";

/**
 * Load the application endpoints
 * @param app
 */
export const initializeRoutes = (app: Application) => {
  app.use(express.json());

  // load index routes
  app.use("/", IndexRouter);

  //Other routes
  app.use("/comment", authorize, CommentsRouter);
};
