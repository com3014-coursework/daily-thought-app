export type Comment = {
  PostId: string;
  AuthorId: string;
  Content: string;
  IsPostAuthor: boolean;
  LikerIds: String[];
};
