import mongoose, { Schema } from "mongoose";

/**
 * Schema for representing a comment
 */
const CommentSchema = new Schema({
  PostId: {
    type: String,
    required: true,
  },
  AuthorId: {
    type: String,
    required: true,
  },
  Content: {
    type: String,
    required: true,
  },
  IsPostAuthor: {
    type: Boolean,
    required: true,
  },
  LikerIds: {
    type: [String],
    required: true,
  },
});

export default CommentSchema;
