import CommentSchema from "../Database/Schemas/CommentSchema";
import FriendSchema from "../Database/Schemas/CommentSchema";
import { Comment } from "../Types/Comment";
import { DataStore } from "./DataStore";

/**
 * Contains actions pertaining to storing and accessing Comments
 */
class CommentDataStore extends DataStore<any> {
  /**
   * Create a new friend relation between two users.
   * @param u1
   * @param u2
   * @returns
   */
  public newComment = async (
    postId: string,
    authorId: string,
    content: string,
    isPostAuthor: boolean
  ): Promise<Comment> => {
    return await this.Model.create({
      PostId: postId,
      AuthorId: authorId,
      Content: content,
      IsPostAuthor: isPostAuthor,
    });
  };

  /**
   * Get all comments for post id
   * @param itemCount
   * @returns
   */
  public getComments = async (postId: string): Promise<Comment[]> => {
    return await this.Model.find({ PostId: postId });
  };

  /**
   * Method to remove a comment
   * @param user1
   * @param user2
   */
  public RemoveComment = async (
    jwtAuthorId: string,
    commentId: string
  ): Promise<void> => {
    const comment = await this.Model.findById(commentId);

    if (comment.AuthorId !== jwtAuthorId) {
      throw new Error("Unauthorised");
    }

    const result = await this.Model.findByIdAndDelete(commentId);
    if (result === null) {
      throw new Error("Could not find comment.");
    }

    return result;
  };

  /**
   * Like a comment.
   * @param id
   * @returns
   */
  public LikeOrUnlikeComment = async (
    commentId: string,
    likerId: string,
    like: boolean
  ): Promise<Comment> => {
    const existingComment = await this.Model.findById(commentId);
    const existingLikes = existingComment.LikerIds;

    if (existingComment === null) {
      throw new Error("Could not find comment");
    }

    if (like && existingLikes.includes(likerId)) {
      throw new Error("Can't Like a comment twice!");
    }

    if (!like && !existingLikes.includes(likerId)) {
      throw new Error("Comment not Liked!");
    }

    if (like) {
      existingLikes.push(likerId);
    } else {
      existingLikes.splice(existingLikes.indexOf(likerId), 1);
    }

    const result = await this.Model.findByIdAndUpdate(commentId, {
      LikerIds: existingLikes,
    });

    if (result === null) {
      throw new Error("Could not find comment");
    }

    return result;
  };
}

export default new CommentDataStore("Comment", CommentSchema);
