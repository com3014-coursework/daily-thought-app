import express, { Response } from "express";
import { CustomJWTRequest, TokenData } from "../Middleware/Auth";
import { CommentManager } from "../Requests/CommentManager";

export const CommentsRouter = express.Router();
const commentManager = new CommentManager();

/**
 * GET '/:postId'
 * Returns a string
 */
CommentsRouter.get(
  "/:postId",
  async (req: CustomJWTRequest, res: Response): Promise<void> => {
    const { token } = req;
    const uid = (token as TokenData).userId;

    return commentManager
      .GetCommentsForPost(req.params.postId)
      .then((result) => {
        res.status(200).json({ result: result });
      })
      .catch((error: Error) => {
        res.status(400).json({ error: error });
      });
  }
);

/**
 * Delete '/'
 * Returns a string
 */
CommentsRouter.delete(
  "/",
  async (req: CustomJWTRequest, res: Response): Promise<Response> => {
    const { commentId, userId } = req.body;
    const { token } = req;
    const uid = (token as TokenData).userId;

    if (uid !== userId) {
      return res.status(400).json({ error: "unauthorised" });
    }

    return commentManager
      .RemoveComment(commentId, uid)
      .then((result) => {
        return res.sendStatus(200);
      })
      .catch((error: Error) => {
        return res.status(400).json({ error: error.message });
      });
  }
);

/**
 * POST /new'
 * Returns a string
 */
CommentsRouter.post(
  "/new",
  async (req: CustomJWTRequest, res: Response): Promise<void> => {
    const { token } = req;
    const uid = (token as TokenData).userId;
    const { postId, userId, content } = req.body;

    return commentManager
      .CreateComment(postId, userId, content)
      .then((result) => {
        res.status(200).json({ result: result });
      })
      .catch((error: Error) => {
        res.status(400).json({ error: error });
      });
  }
);

/**
 * POST /like/:postId'
 * Returns a string
 */
CommentsRouter.post(
  "/like",
  async (req: CustomJWTRequest, res: Response): Promise<Response> => {
    const { token } = req;
    const uid = (token as TokenData).userId;
    const { commentId, userId } = req.body;

    if (uid !== userId) {
      return res.status(400).json({ error: "unauthorised" });
    }

    return commentManager
      .LikeComment(commentId, userId)
      .then((result) => {
        return res.sendStatus(200);
      })
      .catch((error: Error) => {
        return res.status(400).json({ error: error });
      });
  }
);

/**
 * POST /unlike/:postId'
 * Returns a string
 */
CommentsRouter.post(
  "/unlike",
  async (req: CustomJWTRequest, res: Response): Promise<Response> => {
    const { token } = req;
    const uid = (token as TokenData).userId;
    const { commentId, userId } = req.body;

    if (uid !== userId) {
      return res.status(400).json({ error: "unauthorised" });
    }

    return commentManager
      .UnLikeComment(commentId, userId)
      .then((result) => {
        return res.sendStatus(200);
      })
      .catch((error: Error) => {
        return res.status(400).json({ error: error });
      });
  }
);
