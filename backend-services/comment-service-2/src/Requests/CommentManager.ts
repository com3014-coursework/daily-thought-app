import CommentDataStore from "../Datastores/CommentDataStore";
import { Comment } from "../Types/Comment";

/**
 * Handles all comment functionality
 */
export class CommentManager {
  /**
   * Method to create a new comment
   * @param sourceId
   * @param targetId
   * @returns
   */
  public CreateComment = async (
    postId: string,
    authorId: string,
    content: string
  ): Promise<Comment> => {
    return await CommentDataStore.newComment(postId, authorId, content, false);
  };

  /**
   *
   * @param targetId Method to delete a comment
   * @returns
   */
  public RemoveComment = async (
    commentId: string,
    authTokenUid: string
  ): Promise<void> => {
    return await CommentDataStore.RemoveComment(authTokenUid, commentId);
  };

  /**
   *
   * @param targetId Method to get all comments for post
   * @returns
   */
  public GetCommentsForPost = async (postId: string): Promise<Comment[]> => {
    return await CommentDataStore.getComments(postId);
  };

  public LikeComment = async (
    commentId: string,
    likerId: string
  ): Promise<Comment> => {
    return await CommentDataStore.LikeOrUnlikeComment(commentId, likerId, true);
  };

  public UnLikeComment = async (
    commentId: string,
    likerId: string
  ): Promise<Comment> => {
    return await CommentDataStore.LikeOrUnlikeComment(
      commentId,
      likerId,
      false
    );
  };
}
