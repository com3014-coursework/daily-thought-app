import RequestSchema from '../Database/Schemas/RequestSchema';
import { FriendRequest } from '../Types/Request';
import { DataStore } from './DataStore';

/**
 * Contains actions pertaining to storing and accessing Requests
 */
class RequestDataStore extends DataStore<any>{

  public newRequest = async (sourceUserId: string, targetUserId: string): Promise<FriendRequest> => {
    return await this.Model.create({
      SourceUser: sourceUserId,
      TargetUser: targetUserId
    });
  };

  /**
   * Store action for getting requests for a user
   * @param itemCount 
   * @returns 
   */
  public getRequests = async (userId: string): Promise<FriendRequest[]> => {
    return await this.Model.find({TargetUser: userId}).sort({$natural: -1})
  }

   /**
   * Store action for getting requests for a user
   * @param itemCount 
   * @returns 
   */
    public getSentRequests = async (userId: string): Promise<FriendRequest[]> => {
      return await this.Model.find({SourceUser: userId}).sort({$natural: -1})
    }

  /**
   * Find and delete a request if it exists.
   * @param requestId 
   * @returns 
   */
  public handleRequestById = async (requestId: string): Promise<FriendRequest> => {
    const result = await this.Model.findByIdAndDelete(requestId)

    if (result === null){
      throw new Error ("Invalid request id!")
    }
    return result;
  }

  /**
   * Get a request by its id
   * @param requestId 
   * @returns 
   */
  public GetRequestById = async (requestId: string): Promise<FriendRequest> => {
    const result = await this.Model.findById(requestId)

    if(result === null){
      throw new Error ("Invalid request id!")
    }
    
    return result;
  }
}

export default new RequestDataStore('Request', RequestSchema)