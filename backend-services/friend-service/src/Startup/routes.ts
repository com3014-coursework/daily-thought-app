import express, { Application } from 'express'
import { authorize } from '../Middleware/Auth'
import { IndexRouter } from '../Routes'
import { FriendsRouter } from '../Routes/FriendsRouter'
import { RequestRouter } from '../Routes/RequestsRouter'

/**
 * Load the application endpoints
 * @param app 
 */
export const initializeRoutes = (app: Application) => {
  app.use(express.json())

  // load index routes
  app.use('/', IndexRouter)

  //Other routes
  app.use('/friends', authorize, FriendsRouter)
  app.use('/friends/requests', authorize, RequestRouter)
}