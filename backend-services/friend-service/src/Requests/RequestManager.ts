import FriendDataStore from "../Datastores/FriendDataStore";
import RequestDataStore from "../Datastores/RequestDataStore"
import { FriendManager } from "../Friends/FriendManager";
import { Friend } from "../Types/Friend";
import { FriendRequest } from "../Types/Request";

/**
 * Handles all friend request functionality
 */
export class RequestManager {

  private friendManager: FriendManager

  constructor(friendManager: FriendManager){
    this.friendManager = friendManager;
  }

  /**
   * Method to create a new friend request
   * @param sourceId 
   * @param targetId 
   * @returns 
   */
  public NewRequest = async (sourceId: string, targetId: string):Promise<FriendRequest> => {
    if(sourceId === targetId){
      throw new Error("Sorry, can't friend yourself! :(");
    }

    if(await RequestDataStore.GetItem({SourceUser: sourceId, TargetUser: targetId}) !== null || await RequestDataStore.GetItem({SourceUser: targetId, TargetUser: sourceId}) !== null){
      throw new Error("Request already exists!");
    }

    if(await FriendDataStore.GetFriendByUsers(sourceId, targetId) !== null){
      throw new Error("Users are already friends!");
    }
    return await RequestDataStore.newRequest(sourceId, targetId);
  }

  /**
   * 
   * @param targetId Method to get user requests
   * @returns 
   */
  public GetRequests = async (targetId: string):Promise<FriendRequest[]> => {
    return await RequestDataStore.getRequests(targetId)
  }

  /**
   * 
   * @param targetId Method to get user requests
   * @returns 
   */
   public GetSentRequests = async (sourceId: string):Promise<FriendRequest[]> => {
    return await RequestDataStore.getSentRequests(sourceId)
  }

  public AcceptRequest = async (requestId: string): Promise<Friend> => {
    const request = await RequestDataStore.handleRequestById(requestId)
    return await this.friendManager.AddFriend(request.SourceUser, request.TargetUser)
  }

  public RejectRequest = async (requestId: string): Promise<FriendRequest> => {
    return await RequestDataStore.handleRequestById(requestId);
  }

  public GetSingleRequest = async (requestId: string): Promise<FriendRequest> => {
    return await RequestDataStore.GetRequestById(requestId);
  }
}