import express, { Request, Response } from 'express'
import { FriendManager } from '../Friends/FriendManager';
import { CustomJWTRequest, TokenData } from '../Middleware/Auth';
import { RequestManager } from '../Requests/RequestManager'

export const RequestRouter = express.Router();
const friendManager = new FriendManager();
const requestManager = new RequestManager(friendManager);

/**
 * GET '/'
 * Returns a string
 */
RequestRouter.get('/', async (req:CustomJWTRequest, res:Response): Promise<Response> => {
  const {token} = req
  const uid = (token as TokenData).userId

  return requestManager.GetRequests(uid as string).then((result) => {
    return res.status(200).json({requests: result})
  }).catch((error: Error) => {
    return res.status(401).json({error: error.message})
  });
});

/**
 * GET '/sent'
 * Returns a string
 */
 RequestRouter.get('/sent', async (req:CustomJWTRequest, res:Response): Promise<Response> => {
  const {token} = req
  const uid = (token as TokenData).userId

  return requestManager.GetSentRequests(uid as string).then((result) => {
    return res.status(200).json({requests: result})
  }).catch((error: Error) => {
    return res.status(401).json({error: error.message})
  });
});

/**
 * POST '/'
 * Creates a friend request
 */
RequestRouter.post('/', async (req:CustomJWTRequest, res:Response): Promise<Response> => {
  const {receiver_id} = req.body;
  const {token} = req
  const uid = (token as TokenData).userId
  
  return requestManager.NewRequest(uid, receiver_id).then((result) => {
    return res.sendStatus(200)
  }).catch((error:Error) => {
    return res.status(400).json({error: error.message})
  });
});

/**
 * PUT '/accept'
 */
RequestRouter.put('/accept', async (req:CustomJWTRequest, res:Response): Promise<Response> => {
  const {request_id} = req.body;
  const {token} = req
  const uid = (token as TokenData).userId

 return await requestManager.GetSingleRequest(request_id).then(result => {
    if(result.TargetUser !== uid){
      throw new Error("Unauthorised")
    } else {
      return requestManager.AcceptRequest(request_id).then((result) => {
        return res.sendStatus(200)
      }).catch((error: Error) => {
        return res.status(400).json({error: error.message})
      })
    }
  }).catch((error: Error) => {
    return res.status(400).json({error: error.message})
  })
});

/**
 * PUT '/reject'
 */
RequestRouter.put('/reject', async (req:CustomJWTRequest, res:Response): Promise<Response> => {
  const {request_id} = req.body;
  const {token} = req
  const uid = (token as TokenData).userId

  return await requestManager.GetSingleRequest(request_id).then(result => {
    if(result.TargetUser !== uid){
      throw new Error("Unauthorised")
    } else {
      return requestManager.RejectRequest(request_id).then((result) => {
        return res.sendStatus(200)
      }).catch((error: Error) => {
        return res.status(400).json({error: error.message})
      })
    }
  }).catch((error: Error) => {
    return res.status(400).json({error: error.message})
  })
});

