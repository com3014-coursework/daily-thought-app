import express, { Response } from 'express'
import { FriendManager } from '../Friends/FriendManager'
import { CustomJWTRequest, TokenData } from '../Middleware/Auth'

export const FriendsRouter = express.Router()
const friendManager = new FriendManager();

/**
 * GET '/'
 * Returns a string
 */
FriendsRouter.get('/', async (req:CustomJWTRequest, res:Response): Promise<void> => {
  const {token} = req
  const uid = (token as TokenData).userId

  return friendManager.GetFriendIds(uid).then(result => {
    res.status(200).json({result: result});
  }).catch((error: Error) => {
    res.status(400).json({error: error})
  });
});

/**
 * Delete '/'
 * Returns a string
 */
 FriendsRouter.delete('/', async (req:CustomJWTRequest, res:Response): Promise<Response> => {
  const {user_id, friend_id} = req.body
  const {token} = req

  if(token !== user_id){
    return res.status(400).json({error: 'unauthorised'})
  }

  return friendManager.RemoveFriend(user_id, friend_id).then(result => {
    return res.sendStatus(200);
  }).catch((error: Error) => {
    return res.status(400).json({error: error.message})
  });
});