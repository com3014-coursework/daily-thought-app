export type FriendRequest = {
  SourceUser: string,
  TargetUser: string
}