import mongoose, { Schema } from 'mongoose'

/**
 * Schema for representing an a friend
 */
const FriendSchema = new Schema({
  User1: {
    type: String,
    required: true
  },
  User2: { 
    type: String,
    required: true
  }
})

export default FriendSchema;