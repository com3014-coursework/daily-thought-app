import mongoose, { Schema } from 'mongoose'

/**
 * Schema for representing an a request
 */
const RequestSchema = new Schema({
  SourceUser: {
    type: String,
    required: true
  },
  TargetUser: { 
    type: String,
    required: true
  }
})

export default RequestSchema;