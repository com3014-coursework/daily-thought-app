import FriendDataStore from "../Datastores/FriendDataStore";
import { Friend } from "../Types/Friend";

/**
 * Class to handle all friend functionality
 */
export class FriendManager {

  public AddFriend = async (user1: string, user2: string): Promise<Friend> => {
    return await FriendDataStore.newFriend(user1, user2);
  }

  public GetFriendIds = async (userId: string): Promise<string[]> => {
    const friends = await FriendDataStore.getFriends(userId);
    return friends.friends1.map(f => f.User2).concat(friends.friends2.map(f => f.User1));
  }

  public RemoveFriend = async (user1: string, user2: string): Promise<void> => {
    
    if(await FriendDataStore.GetFriendByUsers(user1, user2) === null){
      throw new Error("Users are not friends!");
    }

    return await FriendDataStore.RemoveFriend(user1, user2);
  }
}