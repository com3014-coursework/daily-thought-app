import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import connect from './database/conn.js'
import router from './router/route.js'
import bcrypt from "bcrypt";
import UserModel from "./model/User.model.js";

const app = express();

app.use(express.json());
app.use(cors());
app.use(morgan('tiny'));
app.disable('x-powered-by');

const port = 9000;

//  HTTP GET 
app.get('/', (req, res) => {
    res.status(201).json("Home GET Request")
});

// API Routes
app.use('/api', router)

// Start server only when we have valid connection
connect().then(async () => {
    try {
        // Database Seeding
        if (process.env.ENABLE_USER_DB_SEEDING) {
            try {
                const password = process.env.SEEDED_ADMIN_PASSWORD
                const hashedPassword = await bcrypt.hash(password, 10);
            
                const admin = new UserModel({
                    username: "admin",
                    password: hashedPassword,
                    email: "admin@email.com",
                    profile: "",
                    admin: true
                })
    
                await admin.save()
                console.log("Successfully seeded User Database!")
            }
            catch(error) {
                console.log("Error seeding User Database...")
                console.log(error)
            }
        }
        
        app.listen(port, () => {
            console.log(`Server connected to http://localhost:${port}`);
        })
    } catch (error) {
        console.log(error)
        console.log('Cannot connect to the server')
    }
}).catch(error => {
    console.log("Invalid database connection...!");
})