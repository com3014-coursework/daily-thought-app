import mongoose from "mongoose";
import { MongoMemoryServer } from "mongodb-memory-server";

async function connect(){

    // const mongod = await MongoMemoryServer.create();
    // const getUri = mongod.getUri();

    const uri = process.env.MONGO_URI
    console.log(uri)
    const db = await mongoose.connect(uri);
    console.log("Database Connected")
    return db;

}

export default connect;