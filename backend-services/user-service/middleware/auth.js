import jwt from 'jsonwebtoken';
import ENV from '../config.js'

export default async function Auth(req, res, next){
    try {

        const token = req.headers.authorization.split(" ")[1];
        
        const decodedToken = await jwt.verify(token, process.env.JWT_PRIVATE_KEY);
        req.user = decodedToken;

        next();

    } catch (error) {
        res.status(401).json({ error : "Authentication Failed!"})
    }
}