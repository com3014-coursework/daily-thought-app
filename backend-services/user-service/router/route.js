import { Router } from "express";
import * as controller from '../controllers/appController.js';

import Auth from '../middleware/auth.js';

const router = Router();

/** POST Methods */
router.route('/register').post(controller.register); // Register
router.route('/login').post(controller.verifyUser, controller.login); // Login
router.route('/userlist').post(controller.GetUserList) // Get user list

/** GET Methods */
router.route('/user/:username').get(controller.getUser); // GetUser
router.route("/search").get(controller.Search) // Search


/** PUT Methods */
router.route('/updateuser').put(Auth, controller.updateUser); // is use to update the user profile

export default router;