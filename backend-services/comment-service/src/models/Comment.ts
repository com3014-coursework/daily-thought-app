import { number } from 'joi';
import mongoose, {Document, Schema} from 'mongoose';

export interface IComment {
                     //Commented section used for testing
    user_id:  mongoose.Schema.Types.ObjectId; //number;
    post_id:  mongoose.Schema.Types.ObjectId; //number;
    body: string;
    users_liked: [mongoose.Schema.Types.ObjectId];

}

export interface ICommentModel extends IComment, Document{}

const CommentSchema: Schema = new Schema(
    {   
        user_id:{ type: mongoose.Schema.Types.ObjectId,required: true, ref: 'User', },
        post_id:{ type: mongoose.Schema.Types.ObjectId, required: true, ref: 'Post'},
        body: { type: String, required: true },
        //Need to look at how to do this?
        users_liked: { type : [mongoose.Schema.Types.ObjectId]}
        
    },
    {
        timestamps: true,
        versionkey: false
    }

)

export default mongoose.model<ICommentModel>('Comments', CommentSchema)

