import { NextFunction, Request, Response } from "express";
import mongoose from 'mongoose';
import Comment from "../models/Comment";
import jwt from 'jsonwebtoken';
import extractJWT from "../middleware/extractJWT";
import { config } from "../config/config";
import Logging from "../library/logging";

const createComment = async ( req: Request, res: Response, next: NextFunction,) => {
    
    
    
    const { post_id, user_id, body} = req.body;
    
    const comment = new Comment({
        _id: new mongoose.Types.ObjectId(),
        post_id,
        user_id,
        body
    });
    
    if (post_id == null || post_id === 'undefined') {
        return res.status(400).send({ message: 'Bad Request' });
     }

    if (user_id == null || user_id === 'undefined') {
        return res.status(400).send({ message: 'Bad Request' });
     }

    if (body == null || body === 'undefined') {
        return res.status(400).send({ message: 'Bad Request' });
     }



    //Try Catach Block to test for whether Post Exsits
    /*
    try{
        
        const posts = {post_id : new mongoose.Types.ObjectId(post_id)}
        const post_exist = await Comment.countDocuments(posts, { limit: 1 })
        if(post_exist == 0) {
            return res.status(404).send({message: 'The specified post or user does not exist.'});
        }
    }
    catch{
        return res.status(404).send({message: 'The specified post or user does not exist.'});

    }
    */

    //Try Catch Block to test whether Users Exsits
    /*
    try{
        
        const users = {post_id : new mongoose.Types.ObjectId(user_id)}
        const users_exist = await Comment.countDocuments(users, { limit: 1 })
        if(users_exist == 0) {
            return res.status(404).send({message: 'The specified post or user does not exist.'});
        }
    }
    catch{
        return res.status(404).send({message: 'The specified post or user does not exist.'});

    }
    */


    
   
    const token = req.headers.authorization?.split(' ')[1];
    if (!token) {
        return res.status(401).send({ message: 'Unauthorised' });
      }
    
    

    try {
        const decodedToken = jwt.verify(token, config.server.token.secret );
        const decodeTokenToString = decodedToken;
        const tokenString = JSON.stringify(decodeTokenToString);
        const JSobj = JSON.parse(tokenString).user_id;
        if(JSobj == user_id){
            const comment_1 = await comment
            .save();
            return res.status(201).json({ comment_1 });
        }
        else{
            return res.status(401).send({ message: 'Unauthorised' });
        }
        

    } catch (error) {
        return res.status(400).send({ message: 'Bad Request' });
    }

};


const readAllComments = async (req: Request, res: Response, next: NextFunction) => {
    
    //Responses
    const id = req.params.post_id;
    if (id == null || id === 'undefined') {
        return res.status(400).send({ message: 'Bad Request' });
     }
    


    const token = req.headers.authorization?.split(' ')[1];
    if (!token) {
        
        return res.status(401).send({ message: 'Unauthorised' });
      
    }


    try{
        
        const decodedToken = jwt.verify(token, config.server.token.secret );
        
    }
    catch{
        return res.status(403).send({ message: 'Forbidden' });
    }

    try{
        const posts = {post_id : new mongoose.Types.ObjectId(id)}
        const post_exist = await Comment.countDocuments(posts, { limit: 1 })
        if(post_exist == 0) {
            return res.status(404).send({message: 'Specified Post Not Found.'});
        }
    }
    catch{
        return res.status(404).send({message: 'Specified Post Not Found.'});

    }

   
    try {
        const query = {post_id : new mongoose.Types.ObjectId(id)}
        const comments = await Comment.find(query).limit(50)
        //const comments: object[] = await Comment.find({}, { _id: 1 }).toArray()
            .select('-__v');
        return res.status(200).json({ comments });
    } catch (error) {
        return res.status(500).send({ message: 'Internal server errors' });
    }

}


export default { createComment,  readAllComments}


