import express from "express";
import controller from '../controllers/Comment';

const router = express.Router();

router.post('/create', controller.createComment);
router.get('/:post_id', controller.readAllComments);
router.get('/', controller.readAllComments);
export = router;

