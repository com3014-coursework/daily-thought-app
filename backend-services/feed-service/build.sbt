name := """feed-service"""
organization := "com.daily"

version := "1.0.0"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.13.10"
scalacOptions ++= Seq("-Ywarn-unused")


libraryDependencies += guice
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0" % Test

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "com.daily.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "com.daily.binders._"
libraryDependencies += "org.mongodb.scala" %% "mongo-scala-driver" % "4.3.0"
libraryDependencies += "com.typesafe.play" %% "play-ws" % "2.8.10"
libraryDependencies += "com.typesafe.play" %% "play-ahc-ws-standalone" % "2.1.10"
libraryDependencies += "com.github.jwt-scala" %% "jwt-play-json" % "9.2.0"
