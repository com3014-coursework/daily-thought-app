import scala.concurrent.Future
import javax.inject._
import play.api.inject.ApplicationLifecycle

import utils.ConfigHelper
import repositories.QuestionRepository

// Creates an `ApplicationStart` object once at start-up and registers hook for shut-down.
@Singleton
class ApplicationStart @Inject() (lifecycle: ApplicationLifecycle) {
  println("Starting...")

  if (ConfigHelper.getBoolean("enable.question.db.seeding")) {
    QuestionRepository.seedDatabase()
  }

  // Shut-down hook
  lifecycle.addStopHook { () =>
    Future.successful(())
  }
}
