package repositories

import models.DailyQuestion
import utils.MongoConnection

import utils.ConfigHelper
import org.bson.types.ObjectId

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


class DailyQuestionRepository extends Repository[DailyQuestion] (
    ConfigHelper.getString("mongo.questionService.db"),
    ConfigHelper.getString("mongo.dailyQuestions.collection")
) {
    /**
     * Inserts a DailyQuestion record into the database.
     * 
     * @return A Future containing the inserted DailyQuestion object with the generated ID.
     */
    def insertDailyQuestion(question: DailyQuestion): Future[DailyQuestion] = {
        val result: Future[String] = MongoConnection.insertOne[DailyQuestion](collection, question)

        // Return a DailyQuestion entity with the generated ID
        result.flatMap[DailyQuestion](id => {
            val updatedQuestion: DailyQuestion = question.copy(id = Some(new ObjectId(id)))
            Future.successful(updatedQuestion)
        })
    }
}
