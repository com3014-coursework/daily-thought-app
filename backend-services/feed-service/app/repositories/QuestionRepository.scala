package repositories

import models.Question
import utils.MongoConnection

import utils.ConfigHelper
import org.bson.types.ObjectId

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


class QuestionRepository extends Repository[Question] (
    ConfigHelper.getString("mongo.questionService.db"),
    ConfigHelper.getString("mongo.questions.collection")
) {
    /**
     * Inserts a Question record into the database.
     * 
     * @return A Future containing the inserted Question object with the generated ID.
     */
    def insertQuestion(question: Question): Future[Question] = {
        val result: Future[String] = MongoConnection.insertOne[Question](collection, question)

        // Return a Question entity with the generated ID
        result.flatMap[Question](id => {
            val updatedQuestion: Question = question.copy(id = Some(new ObjectId(id)))
            Future.successful(updatedQuestion)
        })
    }
}

object QuestionRepository {
    def seedDatabase(): Unit = {
        println("Seeding Question Database...")

        try {
            Question.createQuestionAsync("What is your favourite food?")
            Question.createQuestionAsync("What is your dream holiday destination?")
            Question.createQuestionAsync("What is your favourite local spot?")

            println("Successfully seeded Question Database!")
        } catch {
            case ex: Throwable => {
                println("Error seeding Question Database...")
                println(ex)
            }
        }  
    } 
}
