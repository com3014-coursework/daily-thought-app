package repositories

import models.Daily
import utils.MongoConnection

import utils.ConfigHelper
import org.bson.types.ObjectId

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


class DailyRepository extends Repository[Daily] (
    ConfigHelper.getString("mongo.feedService.db"),
    ConfigHelper.getString("mongo.dailies.collection")
) {
    /**
     * Inserts a Daily record into the database.
     * 
     * @return A Future containing the inserted Daily object with the generated ID.
     */
    def insertDaily(daily: Daily): Future[Daily] = {
        val result: Future[String] = MongoConnection.insertOne[Daily](collection, daily)

        // Return a Daily entity with the generated ID
        result.flatMap[Daily](id => {
            val updatedDaily: Daily = daily.copy(id = Some(new ObjectId(id)))
            Future.successful(updatedDaily)
        })
    }
}
