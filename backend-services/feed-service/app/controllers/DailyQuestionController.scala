package controllers

import models.{DailyQuestion}

import javax.inject._
import play.api.mvc._
import play.api.libs.json.{JsValue}

import scala.concurrent.TimeoutException


/**
 * This controller handles all the Daily Question endpoints.
 */
@Singleton
class DailyQuestionController @Inject()(val controllerComponents: ControllerComponents) extends BaseController {

  /**
   * Create an Action to fetch the current Daily Question.
   */
  def getDailyQuestion() = Action {
    println("QuestionController:getDailyQuestion")

    try {
        val question: DailyQuestion = DailyQuestion.getCurrentDailyQuestionAsync()
        val jsonQuestion: JsValue = DailyQuestion.toJson(question)
        Ok(jsonQuestion)
    } catch {
        case _: TimeoutException => BadRequest("Request timed out")
        case ex: Throwable => {
          println(ex.getMessage())
          BadRequest("Exception raised")
        }
    }
  }
}
