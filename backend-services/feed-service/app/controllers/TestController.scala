package controllers

import models.actions.AuthenticatedUserAction

import javax.inject._
import play.api.mvc._
import play.api.libs.json.{JsString, JsArray}
import play.api.libs.json.JsBoolean

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's test page.
 */
@Singleton
class TestController @Inject()(val controllerComponents: ControllerComponents, authenticatedUserAction: AuthenticatedUserAction)
    extends BaseController {

    def getFriends(userId: String) = authenticatedUserAction {
        println("TestController:getFriends")
        println(s"Fetching friends for User with ID {$userId}")

        val response = JsArray(
            Seq(
                JsString("641128f7e80bcd1ba39d04ae"), 
                JsString("641128f7e80bcd1ba39d04af"), 
                JsString("641128f7e80bcd1ba39d04aa")
            )
        )
        
        Ok(response)
    }

    def verifyUser(userId: String) = authenticatedUserAction {
        println("TestController:verifyUser")
        println(s"Verifying User with ID {$userId}")
        
        Ok(JsBoolean(true))
    }
}
