package controllers

import models.{Question}
import models.actions.{AuthenticatedUserAction, AuthenticationRequest}
import models.exceptions.{ConflictException, InvalidRequestBodyException, InvalidQueryParameterException, ForbiddenException}

import javax.inject._
import play.api.mvc._
import play.api.libs.json.{JsValue}
import play.api.libs.json.JsLookupResult

import scala.concurrent.TimeoutException
import org.bson.types.ObjectId


/**
 * This controller handles all the Question endpoints.
 */
@Singleton
class QuestionController @Inject()(val controllerComponents: ControllerComponents, authenticatedUserAction: AuthenticatedUserAction)
    extends BaseController {

  /**
   * Create an Action to get all the Questions in the DB.
   */
  def getQuestions() = authenticatedUserAction { implicit request: AuthenticationRequest[AnyContent] =>
    println("QuestionController:getQuestions")

    try {
        if (!request.isAdmin) throw new ForbiddenException("You must be an admin to get the stored questions.")
        val questions: Seq[Question] = Question.getQuestionsAsync()
        val jsonResult: JsValue = Question.toJson(questions)
        Ok(jsonResult)
    } catch {
        case _: TimeoutException => BadRequest("Request timed out")
        case ex: ForbiddenException => Forbidden(ex.getMessage())
        case ex: Throwable => {
          println(ex.getMessage())
          BadRequest("Exception raised")
        }
    }
  }

  /**
   * Create an Action to insert a Question to the DB.
   */
  def insertQuestion() = authenticatedUserAction { implicit request: AuthenticationRequest[AnyContent] =>
    println("QuestionController:insertQuestion")

    try {
        if (!request.isAdmin) throw new ForbiddenException("You must be an admin to insert a question.")
        val questionText: String = fetchInsertQuestionRequestBody(request.body)
        Question.createQuestionAsync(questionText)
        Created("Inserted question")
    } catch {
        case _: TimeoutException => BadRequest("Request timed out")
        case ex: ConflictException => BadRequest(ex.getMessage())
        case ex: ForbiddenException => Forbidden(ex.getMessage())
        case ex: Throwable => {
          println(ex.getMessage())
          BadRequest("Exception raised")
        }
    }
  }

    /**
   * Create an Action to disable a Question in the DB.
   */
  def disableQuestion(questionId: String) = authenticatedUserAction { implicit request: AuthenticationRequest[AnyContent] =>
    println("QuestionController:disableQuestion")

    try {
        if (!ObjectId.isValid(questionId)) throw new InvalidQueryParameterException("Invalid query parameter ID format: questionId")
        if (!request.isAdmin) throw new ForbiddenException("You must be an admin to disable a question.")

        Question.disableQuestionAsync(new ObjectId(questionId))
        Ok("Disabled question")
    } catch {
        case _: TimeoutException => BadRequest("Request timed out")
        case ex: InvalidQueryParameterException => BadRequest(ex.getMessage())
        case ex: ConflictException => BadRequest(ex.getMessage())
        case ex: ForbiddenException => Forbidden(ex.getMessage())
        case ex: Throwable => {
          println(ex.getMessage())
          BadRequest("Exception raised")
        }
    }
  }

  /**
   * Fetch the needed values from the request body for the liking/unliking a Daily endpoint.
   * 
   * @param requestBody The request's body.
   */
  def fetchInsertQuestionRequestBody(requestBody: AnyContent): String = {
    if (!requestBody.asJson.isDefined) throw new InvalidRequestBodyException("Request body must be in JSON format.")

    val bodyJson = requestBody.asJson.get
    
    fetchJsonBodyString(bodyJson, "questionText")
  }

  /**
   * Fetch the value of the given field name from the JSON.
   * 
   * @param bodyJson The JSON.
   * @param fieldName The field name.
   */
  def fetchJsonBodyValue(bodyJson: JsValue, fieldName: String): JsValue = {
    val value: JsLookupResult = (bodyJson \ fieldName)
    if (!value.isDefined) throw new InvalidRequestBodyException("Missing parameter: " + fieldName)
    value.get
  }

  /**
   * Fetch the String value of the field name from the JSON.
   * 
   * @param bodyJson The JSON.
   * @param fieldName The field name.
   */
  def fetchJsonBodyString(bodyJson: JsValue, fieldName: String): String = {
    fetchJsonBodyValue(bodyJson, fieldName).as[String]
  }
}
