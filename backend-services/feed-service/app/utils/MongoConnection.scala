package utils

import org.mongodb.scala.{MongoClient, MongoDatabase, MongoCollection}
import org.mongodb.scala.model.{Filters}
import com.mongodb.client.result.{InsertOneResult, UpdateResult, DeleteResult}

import org.bson.conversions.Bson
import org.bson.types.ObjectId

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.reflect.ClassTag

import utils.MongoCodecs


/**
 * Mongo helper functions for connecting to and interacting with MongoDB clients, databases, and collections.
 * 
 * All functions return a Future.
 */
object MongoConnection {
    /**
     * Connects to and gets a reference to a MongoDB client.
     * 
     * @param mongoUri The connection string.
     * @return A Future containing the MongoClient instance.
     */
    def getClient(mongoUri: String): Future[MongoClient] = Future {
        MongoClient(mongoUri)
    }

    /**
     * Gets a reference to a MongoDB database.
     * 
     * @param database The name of the database to retrieve.
     * @return A Future containing a MongoDatabase instance.
     */
    def getDatabase(client: MongoClient, database: String): Future[MongoDatabase] = Future {
        client.getDatabase(database).withCodecRegistry(MongoCodecs.codecRegistry)
    }

    /**
     * Gets a reference to a MongoDB collection within a database.
     * 
     * @param database The MongoDatabase instance containing the desired collection.
     * @param collection The name of the collection to retrieve.
     * @return A Future containing a MongoCollection instance.
     */
    def getCollection[T: ClassTag](database: MongoDatabase, collection: String): Future[MongoCollection[T]] = Future {
        database.getCollection[T](collection)
    }

    /**
     * Finds documents in a MongoDB collection.
     * 
     * @param collection The MongoCollection instance to search.
     * @param filter An optional Bson filter to apply to the search.
     * @param sort An optional Bson sort to apply to the search.
     * @param limit An optional integer limit to apply to the search.
     * @param projection An optional Bson projection to apply to the search.
     * @return A Future containing a sequence of matching documents as Documents.
     */
    def find[T: ClassTag](
        collection: MongoCollection[T], 
        filter: Option[Bson] = None, 
        sort: Option[Bson] = None,
        limit: Option[Int] = None,
        projection: Option[Bson] = None
    ): Future[Seq[T]] = {
        var result = collection.find[T]()

        // Apply parameters which are defined to the result
        result = filter.fold(result)(result.filter)
        result = sort.fold(result)(result.sort)
        result = limit.fold(result)(result.limit)
        result = projection.fold(result)(result.projection)

        result.toFuture()
    }

    /**
     * Inserts a document into a MongoDB collection.
     * 
     * @param collection The MongoCollection instance to insert into.
     * @param document The document to insert.
     * @return A Future containing the ID of the inserted document.
     * @throws RuntimeException if the insertion was not acknowledged by the database.
     */
    def insertOne[T](collection: MongoCollection[T], document: T): Future[String] = {
        val futureResult: Future[InsertOneResult] = collection.insertOne(document).toFuture()
        
        futureResult.map[String]((result: InsertOneResult) => {
            if (result.wasAcknowledged()) {
                // Grab the generated ID of the inserted document
                result.getInsertedId().asObjectId.getValue().toString()
            } else {
                throw new RuntimeException("Insertion was not acknowledged")
            }
        })
    }

    /**
     * Updates a document in a MongoDB collection.
     * 
     * @param collection The MongoCollection instance the document is in.
     * @param documentId The ID of the document to update.
     * @param updates A sequence of Bson documents defining the updates.
     * @throws RuntimeException if the update was not acknowledged by the database.
     */
    def updateOne[T](collection: MongoCollection[T], documentId: ObjectId, updates: Seq[Bson]): Future[Unit] = {
        val filter: Bson = Filters.equal[ObjectId]("_id", documentId)
        val futureResult: Future[UpdateResult] =  collection.updateOne(filter, updates).toFuture()
        
        futureResult.map[Unit]((result: UpdateResult) => {
            if (result.getMatchedCount == 0) {
                throw new RuntimeException("No document with the given ID.")
            }

            if (!result.wasAcknowledged()) {
                throw new RuntimeException("Update was not acknowledged.")
            }

            if (result.getModifiedCount() == 0) {
                throw new RuntimeException("No document was modified.")
            }
        })
    }

    /**
     * Delete one document from the collection that matches the given ID.
     * 
     * @param collection The MongoCollection instance the document is in.
     * @param documentId The ID of the document to delete.
     * @throws RuntimeException if the delete was not acknowledged by the database.
     */
    def deleteOne[T](collection: MongoCollection[T], documentId: ObjectId): Future[Unit] = {
        val filter: Bson = Filters.equal[ObjectId]("_id", documentId)
        val futureResult: Future[DeleteResult] =  collection.deleteOne(filter).toFuture()
        
        futureResult.map[Unit]((result: DeleteResult) => {
            if (!result.wasAcknowledged()) {
                throw new RuntimeException("Delete was not acknowledged.")
            }

            if (result.getDeletedCount() == 0) {
                throw new RuntimeException("No document was deleted.")
            }
        })
    }
}
