package utils

import models.{Daily, Question, DailyQuestion}
import org.bson.codecs.Codec
import org.bson.codecs.configuration.{CodecProvider, CodecRegistry, CodecRegistries}

import org.mongodb.scala.MongoClient

object MongoCodecs {
    // Define a custom CodecProvider that returns a Codec for the case classes
    val customCodecProvider = new CodecProvider {
        override def get[T](clazz: Class[T], registry: CodecRegistry): Codec[T] = {
            if (clazz == classOf[Daily]) {
                // If the class is the Daily case class, return the Daily codec
                Daily.codec.asInstanceOf[Codec[T]]
            }
            else if (clazz == classOf[Question]) {
                // If the class is the Question case class, return the Question codec
                Question.codec.asInstanceOf[Codec[T]]
            }
            else if (clazz == classOf[DailyQuestion]) {
                // If the class is the DailyQuestion case class, return the DailyQuestion codec
                DailyQuestion.codec.asInstanceOf[Codec[T]]
            }
            else {
                // If the class is not the User case class, return null
                null
            }
        }
    }

    // Create a CodecRegistry that includes the custom CodecProvider and the default codecs
    val codecRegistry: CodecRegistry = CodecRegistries.fromProviders(customCodecProvider, MongoClient.DEFAULT_CODEC_REGISTRY)
}
