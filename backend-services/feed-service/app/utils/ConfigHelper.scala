package utils

import com.typesafe.config.{Config, ConfigFactory}

object ConfigHelper {
  private val applicationConfig: Config = ConfigFactory.load("application.conf")
  private val referenceConfig: Config = ConfigFactory.parseResources("reference.conf")
  private val config: Config = referenceConfig.withFallback(applicationConfig).resolve()

  def getString(key: String): String = config.getString(key)
  def getInt(key: String): Int = config.getInt(key)
  def getBoolean(key: String): Boolean = config.getBoolean(key)
}
