package models

import repositories.{QuestionRepository}
import models.exceptions.{ConflictException}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, Await}
import scala.concurrent.duration._

import org.bson.types.ObjectId
import java.util.Date
import java.time.Instant

import org.bson.{BsonWriter, BsonReader}
import org.bson.codecs.{Codec, EncoderContext, DecoderContext}
import org.bson.conversions.Bson

import org.mongodb.scala.model.{Filters, Updates, Sorts}

import play.api.libs.json.{Json, JsValue, JsString, JsObject, JsNumber, JsBoolean, JsArray}
import scala.math.BigDecimal
import java.text.SimpleDateFormat


case class Question (
    id: Option[ObjectId],
    content: String,
    used: Integer,
    disabled: Boolean,
    createdAt: Date,
    updatedAt: Date
)

object Question {
    val questionRepo = new QuestionRepository()

    def getQuestionsAsync(timeout: Int = 4): Seq[Question] = {
        val result: Future[Seq[Question]] = questionRepo.getAll(Some(Filters.eq("disabled", false)), Some(Sorts.descending("createdAt")), None, None)
        Await.result[Seq[Question]](result, timeout.seconds)
    }

    def createQuestionAsync(content: String, timeout: Int = 4): Question = {
        val now: Date = Date.from(Instant.now())
        val question: Question = Question(None, content, 0, false, now, now)

        val newQuestion = for {
            questionExists <- questionRepo.getAll(Some(Filters.eq("content", question.content)), None, None, None).map(_.isEmpty)
            _ = if (!questionExists) throw new ConflictException("Question already exists.")
            newQuestion: Question <- questionRepo.insertQuestion(question)
        } yield newQuestion

        Await.result[Question](newQuestion, timeout.seconds)
    }

    def disableQuestionAsync(questionId: ObjectId, timeout: Int = 4): Unit = {
        val update: Bson = Updates.set("disabled", true)
        val disable: Future[Unit] = questionRepo.updateOne(questionId, Seq(update))

        Await.result[Unit](disable, timeout.seconds)
    }

    def getLeastUsed(): Future[Question] = {
        questionRepo.getFirst(None, Some(Sorts.ascending("used")), None).map((question: Option[Question]) => {
            if (question.isEmpty)
                throw new RuntimeException("No questions stored.")

            question.get
        })
    }

    def useQuestion(question: Question): Future[Unit] = {
        val update: Bson = Updates.set("used", question.used + 1)
        questionRepo.updateOne(question.id.get, Seq(update))
    }

    // Convert from Question object to JSON (serializing to JSON)
    def toJson(question: Question): JsValue = {
        val dateFormat: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        val formattedCreatedAt: String = dateFormat.format(question.createdAt)
        val formattedUpdatedAt: String = dateFormat.format(question.updatedAt)

        val questionJson = Seq(
            "id" -> JsString(question.id.getOrElse("").toString()),
            "content" -> JsString(question.content),
            "used" -> JsNumber(BigDecimal(question.used)),
            "disabled" -> JsBoolean(question.disabled),
            "createdAt" -> JsString(formattedCreatedAt),
            "updatedAt" -> JsString(formattedUpdatedAt)
        )
        
        Json.toJson[JsObject](JsObject(questionJson))
    }

    // Convert from Question set to JSON (serializing to JSON)
    def toJson(questions: Seq[Question]): JsValue = {
        val questionsJson: Seq[JsValue] = questions.map(question => Question.toJson(question))

        Json.toJson[JsArray](JsArray(questionsJson))
    }

    // Codec instance for serialising/deserialising type Question to or from BSON.
    // Implicit keyword lets Scala compiler automatically insert this into the code where it's needed.
    implicit val codec: Codec[Question] = new Codec[Question] {
        override def encode(writer: BsonWriter, value: Question, encoderContext: EncoderContext): Unit = {
            writer.writeStartDocument()
            writer.writeString("content", value.content)
            writer.writeInt32("used", value.used)
            writer.writeBoolean("disabled", value.disabled)
            writer.writeDateTime("createdAt", value.createdAt.getTime())
            writer.writeDateTime("updatedAt", value.updatedAt.getTime())
            writer.writeEndDocument()
        }

        override def decode(reader: BsonReader, decoderContext: DecoderContext): Question = {
            reader.readStartDocument()
            val id = reader.readObjectId("_id")
            val content = reader.readString("content")
            val used = reader.readInt32("used")
            val disabled = reader.readBoolean("disabled")
            val createdAt = reader.readDateTime("createdAt")
            val updatedAt = reader.readDateTime("updatedAt")
            reader.readEndDocument()

            val createdAtDate: Date = Date.from(Instant.ofEpochMilli(createdAt))
            val updatedAtDate: Date = Date.from(Instant.ofEpochMilli(updatedAt))

            Question(Some(id), content, used, disabled, createdAtDate, updatedAtDate)
        }

        override def getEncoderClass: Class[Question] = classOf[Question]
    }
}
