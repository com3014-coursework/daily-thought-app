package models

import models.{Question}
import repositories.{DailyQuestionRepository}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, Await}
import scala.concurrent.duration._

import utils.ConfigHelper

import org.bson.types.ObjectId
import java.util.Date
import java.time.{Instant, ZoneId, ZonedDateTime}
import scala.util.Random

import play.api.libs.json.{Json, JsValue, JsString, JsObject}
import java.text.SimpleDateFormat

import org.bson.{BsonWriter, BsonReader}
import org.bson.codecs.{Codec, EncoderContext, DecoderContext}

import org.mongodb.scala.model.{Filters, Sorts}
import org.bson.conversions.Bson


/**
 * Represents a daily question that is given to all users.
 * 
 * @param id The unique ID of the daily question object.
 * @param questionId The unique ID of the question that is associated with this daily question.
 * @param questionText The text of the question that is associated with this daily question. Duplicated from the Question object to reduce database hits.
 * @param nextQuestionAt The date and time of when the question expires.
 * @param createdAt The date and time when the daily question object was created.
 * @param updatedAt The date and time when the daily question object was last updated.
 */
case class DailyQuestion (
    id: Option[ObjectId],
    questionId: ObjectId,
    questionText: String,
    nextQuestionAt: Date,
    createdAt: Date,
    updatedAt: Date
)

object DailyQuestion {
    val dailyQuestionRepo = new DailyQuestionRepository()

    def createDailyQuestion(): Future[DailyQuestion] = {        
        for {
            question: Question <- Question.getLeastUsed()
            _ <- Question.useQuestion(question)

            insertedDailyQuestion: DailyQuestion <- {
                val now: Date = Date.from(Instant.now())
                val nextQuestionAt = getRandomTime()
                val dailyQuestion: DailyQuestion = DailyQuestion(None, question.id.get, question.content, nextQuestionAt, now, now)
                dailyQuestionRepo.insertDailyQuestion(dailyQuestion)
            }

        } yield insertedDailyQuestion
    }

    def getCurrentDailyQuestion(): Future[DailyQuestion] = {
        val now: Date = Date.from(Instant.now())
        val filter: Bson = Filters.gt[Date]("nextQuestionAt", now)
        val sort: Bson = Sorts.descending("nextQuestionAt")
        
        for {
            existingQuestion: Option[DailyQuestion] <- dailyQuestionRepo.getFirst(Some(filter), Some(sort), None)

            newQuestion: DailyQuestion <- {
                if (existingQuestion.isEmpty)
                    createDailyQuestion()
                else
                    Future.successful(existingQuestion.get)
            }

        } yield newQuestion
    }

    def getCurrentDailyQuestionAsync(timeout: Int = 4): DailyQuestion = {
        val currentDailyQuestion: Future[DailyQuestion] = getCurrentDailyQuestion()

        Await.result[DailyQuestion](currentDailyQuestion, timeout.seconds)
    }

    def getRandomTime(): Date = {
        val minHour = ConfigHelper.getInt("dailyQuestions.min.hour")
        val maxHour = ConfigHelper.getInt("dailyQuestions.max.hour")

        val randomHour = Random.between(minHour, maxHour + 1)
        val randomMinute = Random.between(0, 60)

        val nextQuestionAt = ZonedDateTime.now(ZoneId.systemDefault()).plusDays(1).withHour(randomHour).withMinute(randomMinute).withSecond(0)

        Date.from(nextQuestionAt.toInstant())
    }

    // Convert from Daily object to JSON (serializing to JSON)
    def toJson(dailyQuestion: DailyQuestion): JsValue = {
        val dateFormat: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        val formattedNextQuestionAt: String = dateFormat.format(dailyQuestion.nextQuestionAt)
        val formattedCreatedAt: String = dateFormat.format(dailyQuestion.createdAt)
        val formattedUpdatedAt: String = dateFormat.format(dailyQuestion.updatedAt)

        val dailyQuestionJson = Seq(
            "id" -> JsString(dailyQuestion.id.getOrElse("").toString()),
            "questionId" -> JsString(dailyQuestion.questionId.toString()),
            "question" -> JsString(dailyQuestion.questionText),
            "nextQuestionAt" -> JsString(formattedNextQuestionAt),
            "createdAt" -> JsString(formattedCreatedAt),
            "updatedAt" -> JsString(formattedUpdatedAt)
        )
        
        Json.toJson[JsObject](JsObject(dailyQuestionJson))
    }

    // Codec instance for serialising/deserialising type DailyQuestion to or from BSON.
    // Implicit keyword lets Scala compiler automatically insert this into the code where it's needed.
    implicit val codec: Codec[DailyQuestion] = new Codec[DailyQuestion] {
        override def encode(writer: BsonWriter, value: DailyQuestion, encoderContext: EncoderContext): Unit = {
            writer.writeStartDocument()
            writer.writeObjectId("questionId", value.questionId)
            writer.writeString("questionText", value.questionText)
            writer.writeDateTime("nextQuestionAt", value.nextQuestionAt.getTime())
            writer.writeDateTime("createdAt", value.createdAt.getTime())
            writer.writeDateTime("updatedAt", value.updatedAt.getTime())
            writer.writeEndDocument()
        }

        override def decode(reader: BsonReader, decoderContext: DecoderContext): DailyQuestion = {
            reader.readStartDocument()
            val id: ObjectId = reader.readObjectId("_id")
            val questionId: ObjectId = reader.readObjectId("questionId")
            val questionText: String = reader.readString("questionText")
            val nextQuestionAt: Long = reader.readDateTime("nextQuestionAt")
            val createdAt: Long = reader.readDateTime("createdAt")
            val updatedAt: Long = reader.readDateTime("updatedAt")
            reader.readEndDocument()

            val nextQuestionAtDate: Date = Date.from(Instant.ofEpochMilli(nextQuestionAt))
            val createdAtDate: Date = Date.from(Instant.ofEpochMilli(createdAt))
            val updatedAtDate: Date = Date.from(Instant.ofEpochMilli(updatedAt))

            DailyQuestion(Some(id), questionId, questionText, nextQuestionAtDate, createdAtDate, updatedAtDate)
        }

        override def getEncoderClass: Class[DailyQuestion] = classOf[DailyQuestion]
    }
}
