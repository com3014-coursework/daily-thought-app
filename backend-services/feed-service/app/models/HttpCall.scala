package models

import akka.actor.ActorSystem
import akka.stream.{Materializer, SystemMaterializer}
import play.api.libs.ws.ahc.{StandaloneAhcWSClient}
import play.api.libs.ws.StandaloneWSRequest
import play.api.libs.json.JsValue

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global


object HttpCall {

    /**
     * Fetches the response of a GET request to the given URL.
     * 
     * @param url The URL of the request.
     * 
     * @return a Future containing the response body as a JSON.
     */
    def get(
          url: String, 
          queryStringParameters: Seq[(String, String)] = Seq.empty[(String, String)], 
          jwt: String = ""
      ): Future[JsValue] = {
        // Create ActorSystem for thread and streaming management
        implicit val system: ActorSystem = ActorSystem()

        // Materializer ensures streams are executed correctly and resources are managed efficiently
        implicit val materializer: Materializer = SystemMaterializer(system).materializer

        // Create the standalone WS client
        val wsClient: StandaloneAhcWSClient = StandaloneAhcWSClient()

        // Create base request
        var request: StandaloneWSRequest = wsClient.url(url)

        // Add query parameters to request
        request = request.addQueryStringParameters(queryStringParameters: _*)

        // Add JWT header to request, if supplied
        request = if (!jwt.isEmpty()) request.addHttpHeaders(("Authorization", s"Bearer $jwt")) else request

        // Call API and fetch response
        val response: Future[JsValue] = request.get().map(response => {
            if (response.status > 399) throw new RuntimeException()

            val statusText: String = response.statusText
            println(s"Got a response: $statusText")
            response.body[JsValue]
        })

        // Close WSClient and terminate ActorSystem
        response
            .andThen { case _ => wsClient.close() }
            .andThen { case _ => system.terminate() }
    }
}
