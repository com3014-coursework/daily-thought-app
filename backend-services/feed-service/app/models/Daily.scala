package models

import repositories.{DailyRepository}
import models.exceptions.{ConflictException, NotFoundException}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, Await}
import scala.concurrent.duration._

import org.bson.types.ObjectId
import java.util.Date
import java.time.Instant
import java.text.SimpleDateFormat

import play.api.libs.json.{Json, JsValue, JsString, JsObject, JsArray, JsNull}

import org.bson.{BsonWriter, BsonReader, BsonType}
import org.bson.codecs.{Codec, EncoderContext, DecoderContext}
import org.bson.conversions.Bson
import org.mongodb.scala.model.{Updates, Filters, Sorts}

case class Daily(
    id: Option[ObjectId],
    userId: ObjectId,
    questionId: ObjectId,
    content: String,
    usersLiked: Seq[ObjectId],
    createdAt: Date,
    updatedAt: Date
)

object Daily {
    val dailyRepo = new DailyRepository()

    def createDailyAsync(
        userId: ObjectId,
        questionId: ObjectId,
        content: String,
        timeout: Int = 4
    ): Daily = {
        val now: Date = Date.from(Instant.now())
        val daily: Daily = Daily(None, userId, questionId, content, Seq.empty[ObjectId], now, now)
        val future: Future[Daily] = dailyRepo.insertDaily(daily)
        Await.result[Daily](future, timeout.seconds)
    }

    def getAllDailiesAsync(timeout: Int = 4): Seq[Daily] = {
        val future: Future[Seq[Daily]] = dailyRepo.getAll()
        Await.result[Seq[Daily]](future, timeout.seconds)
    }

    def getUserDailiesAsync(userId: ObjectId, timeout: Int = 4): Seq[Daily] = {
        val future: Future[Seq[Daily]] = dailyRepo.getByValue("user_id", userId)
        Await.result[Seq[Daily]](future, timeout.seconds)
    }

    def getUserFeedAsync(userId: ObjectId, questionId: ObjectId, jwt: String, timeout: Int = 4): (Seq[Daily], Option[Daily]) = {
        val result = for {
            friends: Seq[ObjectId] <- User.getUserFriends(userId, jwt)
            userDaily: Option[Daily] <- getUserDaily(userId, questionId)
            feed: Seq[Daily] <- {
                val friendsFilter: Bson = Filters.in("user_id", friends: _*)
                val questionFilter: Bson = Filters.eq("question_id", questionId)
                val filters: Bson = Filters.and(friendsFilter, questionFilter)
                val sort: Bson = Sorts.descending("createdAt")

                val futureFeed: Future[Seq[Daily]] = dailyRepo.getAll(Some(filters), Some(sort), None, None)

                if (userDaily.isEmpty) 
                    futureFeed.map(_.map(_.copy(content = "")))
                else 
                    futureFeed
            }
        } yield (feed: Seq[Daily], userDaily: Option[Daily])

        Await.result(result, timeout.seconds)
    }

    def getCurrentUserDailyAsync(userId: ObjectId, timeout: Int = 4): Option[Daily] = {
        val userDaily: Future[Option[Daily]] = for {
            questionId: ObjectId <- DailyQuestion.getCurrentDailyQuestion().map(_.id.get)
            userDaily <- {
                val userFilter: Bson = Filters.eq("user_id", userId)
                val questionFilter: Bson = Filters.eq("question_id", questionId)
                val filters: Bson = Filters.and(userFilter, questionFilter)

                dailyRepo.getFirst(Some(filters), None, None)
            }
        } yield userDaily

        Await.result(userDaily, timeout.seconds)
    }

    def getUserDaily(userId: ObjectId, questionId: ObjectId): Future[Option[Daily]] = {
        val userFilter: Bson = Filters.eq("user_id", userId)
        val questionFilter: Bson = Filters.eq("question_id", questionId)
        val filters: Bson = Filters.and(userFilter, questionFilter)

        dailyRepo.getFirst(Some(filters), None, None)
    }

    def likeAsync(dailyId: ObjectId, likerId: ObjectId, jwt: String, timeout: Int = 4): Unit = {
        val result: Future[Unit] = for {
            // Fetch Daily from given ID
            daily: Daily  <- {
                dailyRepo.getById(dailyId).map((oDaily: Option[Daily]) => {
                    if (oDaily.isEmpty) 
                        throw new NotFoundException("No daily with given ID.") 
                    else 
                        oDaily.get
                })
            }

            // Check user has not already liked the Daily
            _ = if (daily.usersLiked.contains(likerId)) throw new ConflictException("User has already liked this Daily.")

            // Check user with given ID exists
            _ <- User.userExists(likerId, jwt).map((exists: Boolean) => if (!exists) throw new NotFoundException("No user with given ID."))

            like: Unit <- {
                val updatedUsersLiked: Seq[ObjectId] = daily.usersLiked :+ likerId
                val update: Bson = Updates.set("usersLiked", updatedUsersLiked)

                dailyRepo.updateOne(dailyId, Seq(update))
            }
        } yield like

        Await.result[Unit](result, timeout.seconds)
    }

    def unlikeAsync(dailyId: ObjectId, likerId: ObjectId, jwt: String, timeout: Int = 4): Unit = {
        val result: Future[Unit] = for {
            // Fetch Daily from given ID
            daily: Daily  <- {
                dailyRepo.getById(dailyId).map((oDaily: Option[Daily]) => {
                    if (oDaily.isEmpty) 
                        throw new NotFoundException("No daily with given ID.") 
                    else 
                        oDaily.get
                })
            }

            // Check user with given ID exists
            _ <- User.userExists(likerId, jwt).map((exists: Boolean) => if (!exists) throw new NotFoundException("No user with given ID."))

            // Check user has liked the Daily
            _ = if (!daily.usersLiked.contains(likerId)) throw new ConflictException("User has not liked this Daily.")

            unlike: Unit <- {
                val updatedUsersLiked: Seq[ObjectId] = daily.usersLiked.filterNot(_ == likerId)
                val update: Bson = Updates.set("usersLiked", updatedUsersLiked)

                dailyRepo.updateOne(dailyId, Seq(update))
            }
        } yield unlike

        Await.result[Unit](result, timeout.seconds)
    }

    // Convert from Daily object to JSON (serializing to JSON)
    def toJson(daily: Daily): JsValue = {
        val usersLikedAsJsStrings: Seq[JsString] = daily.usersLiked.map[JsString](id => JsString(id.toString()))

        val dateFormat: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        val formattedCreatedAt: String = dateFormat.format(daily.createdAt)
        val formattedUpdatedAt: String = dateFormat.format(daily.updatedAt)

        val dailyJson = Seq(
            "id" -> JsString(daily.id.getOrElse("").toString()),
            "userId" -> JsString(daily.userId.toString()),
            "questionId" -> JsString(daily.questionId.toString()),
            "content" -> JsString(daily.content),
            "usersLiked" -> JsArray(usersLikedAsJsStrings),
            "createdAt" -> JsString(formattedCreatedAt),
            "updatedAt" -> JsString(formattedUpdatedAt)
        )
        
        Json.toJson[JsObject](JsObject(dailyJson))
    }

    // Convert from Daily set to JSON (serializing to JSON)
    def toJson(dailies: Seq[Daily]): JsValue = {
        val dailiesJson: Seq[JsValue] = dailies.map(daily => Daily.toJson(daily))

        Json.toJson[JsArray](JsArray(dailiesJson))
    }

    // Convert from optional Daily to JSON (serializing to JSON)
    def optionalToJson(daily: Option[Daily]): JsValue =
        daily.fold[JsValue](JsNull)(Daily.toJson)

    // Convert from feed set to JSON (serializing to JSON)
    def feedToJson(dailies: Seq[Daily], userDaily: Option[Daily]): JsValue = {
        val jsonDailies: JsValue = toJson(dailies)
        val jsonUserDaily: JsValue = userDaily.fold[JsValue](JsNull)(Daily.toJson)
        Json.toJson[JsObject](JsObject(Seq("userDaily" -> jsonUserDaily, "feed" -> jsonDailies)))
    }

    def toString(daily: Daily): String =
        return s"Daily(${daily.id.toString()}, ${daily.userId.toString()}, ${daily.questionId.toString()}, ${daily.content}, ${daily.usersLiked.toString()})"

    // Codec instance for serialising/deserialising type User to or from BSON.
    // Implicit keyword lets Scala compiler automatically insert this into the code where it's needed.
    implicit val codec: Codec[Daily] = new Codec[Daily] {
        override def encode(writer: BsonWriter, value: Daily, encoderContext: EncoderContext): Unit = {
            writer.writeStartDocument()
            writer.writeObjectId("user_id", value.userId)
            writer.writeObjectId("question_id", value.questionId)
            writer.writeString("content", value.content)
            writer.writeStartArray("usersLiked")
            value.usersLiked.foreach(writer.writeObjectId)
            writer.writeEndArray()
            writer.writeDateTime("createdAt", value.createdAt.getTime())
            writer.writeDateTime("updatedAt", value.updatedAt.getTime())
            writer.writeEndDocument()
        }

        override def decode(reader: BsonReader, decoderContext: DecoderContext): Daily = {
            reader.readStartDocument()
            val id = reader.readObjectId("_id")
            val userId = reader.readObjectId("user_id")
            val questionId = reader.readObjectId("question_id")
            val content = reader.readString("content")
            val usersLiked = {
                reader.readName("usersLiked")
                reader.readStartArray()
                val buffer = collection.mutable.Buffer.empty[ObjectId]
                while (reader.readBsonType() != BsonType.END_OF_DOCUMENT) {
                    buffer += reader.readObjectId()
                }
                reader.readEndArray()
                buffer.toSeq
            }
            val createdAt = reader.readDateTime("createdAt")
            val updatedAt = reader.readDateTime("updatedAt")
            reader.readEndDocument()

            val createdAtDate: Date = Date.from(Instant.ofEpochMilli(createdAt))
            val updatedAtDate: Date = Date.from(Instant.ofEpochMilli(updatedAt))

            Daily(Some(id), userId, questionId, content, usersLiked, createdAtDate, updatedAtDate)
        }

        override def getEncoderClass: Class[Daily] = classOf[Daily]
    }
}
