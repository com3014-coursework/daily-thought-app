package models

import models.HttpCall

import play.api.libs.json.JsValue

import utils.ConfigHelper
import org.bson.types.ObjectId

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global


object User {
    def getUserFriends(userId: ObjectId, jwt: String): Future[Seq[ObjectId]] = {
        val friendServiceUri: String = ConfigHelper.getString("friend.service.uri")

        val url: String = s"${friendServiceUri}friends"
        val queryStringParameters: Seq[(String, String)] = Seq(("userId", userId.toString()))
        
        HttpCall.get(url, queryStringParameters, jwt).map[Seq[ObjectId]]((json: JsValue) => {
            val result = (json \ "result").as[JsValue]
            val sequence: Seq[String] = result.as[Seq[String]]
            println(sequence)
            sequence.map[ObjectId](new ObjectId(_))
        })
    }

    def userExists(userId: ObjectId, jwt: String): Future[Boolean] = {
        val userServiceUri: String = ConfigHelper.getString("user.service.uri")

        val url: String = s"${userServiceUri}test/verifyUser"
        val queryStringParameters: Seq[(String, String)] = Seq(("userId", userId.toString()))
        
        HttpCall.get(url, queryStringParameters, jwt).map[Boolean](_.as[Boolean])
    }
}
