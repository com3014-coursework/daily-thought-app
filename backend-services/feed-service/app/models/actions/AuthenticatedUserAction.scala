package models.actions

import models.actions.AuthenticationRequest

import play.api.mvc.{ActionBuilder, BodyParsers, Request, Result, AnyContent}

import javax.inject.Inject
import scala.concurrent.{Future, ExecutionContext}


/**
 * The authentication action builder that combines the request transformation and filtering.
 */
class AuthenticatedUserAction @Inject()(authenticationTransformer: AuthenticationTransformer, authenticationFilter: AuthenticationFilter)
                                       (implicit val parser: BodyParsers.Default, val executionContext: ExecutionContext)
    extends ActionBuilder[AuthenticationRequest, AnyContent] {

    /**
     * Invoke the main controller block, with the transformations and filtering middleware, and the CSRF token injection.
     *
     * @param request The incoming request.
     * @param block The block of code to invoke.
     * @return A future of the result.
     */
    override def invokeBlock[A](request: Request[A], block: AuthenticationRequest[A] => Future[Result]): Future[Result] = {
        (authenticationTransformer andThen authenticationFilter).invokeBlock(request, block)
    }
}
