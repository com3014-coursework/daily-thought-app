package models.actions

import models.actions.AuthenticationRequest
import play.api.mvc.{ActionFilter, Result, Results}

import scala.concurrent.{Future, ExecutionContext}

import javax.inject.Inject


/**
 * The authentication action filter that verifies the request contains a user ID.
 */
class AuthenticationFilter @Inject() (implicit val executionContext: ExecutionContext) extends ActionFilter[AuthenticationRequest] {
    
    /**
     * Determines whether to process a request.
     * Decides whether to immediately intercept the request or continue processing the request.
     *
     * @param request The incoming request.
     * @return An optional Forbidden Result with which to abort the request.
     */
    override def filter[A](request: AuthenticationRequest[A]): Future[Option[Result]] = Future.successful {
        if (!request.requesterId.isDefined) 
            Some(Results.Forbidden("Invalid JWT Token"))
        else
            None
    }
}
