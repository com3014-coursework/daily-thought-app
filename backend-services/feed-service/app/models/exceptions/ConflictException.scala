package models.exceptions

case class ConflictException(message: String) extends Exception(message)
