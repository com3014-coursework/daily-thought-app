package models.exceptions

case class InvalidQueryParameterException(message: String) extends Exception(message)
