package models.exceptions

case class InvalidObjectIdValueException(message: String) extends Exception(message)
