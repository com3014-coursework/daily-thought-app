package models.exceptions

case class InvalidRequestBodyException(message: String) extends Exception(message)
