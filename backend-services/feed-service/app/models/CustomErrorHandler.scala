package models

import javax.inject.Singleton
import play.api.http.HttpErrorHandler
import play.api.mvc.{RequestHeader, Results, Result}
import scala.concurrent.Future

@Singleton
class CustomErrorHandler extends HttpErrorHandler {

    def onClientError(request: RequestHeader, statusCode: Int, message: String): Future[Result] = {
        Future.successful(Results.Status(statusCode)(message))
    }

    def onServerError(request: RequestHeader, exception: Throwable): Future[Result] = {
        Future.successful(Results.InternalServerError("A server error occurred: " + exception.getMessage()))
    }
}
